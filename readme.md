Pipeline for lncRNAs conservation study.
Tested on Ubuntu 16.04 LTS


###Attached binary files with the links to sources
./slncky			https://github.com/slncky/slncky
./src/bedtools2			https://github.com/arq5x/bedtools2 
./src/lastz-distrib		http://www.bx.psu.edu/~rsharris/lastz/
./src/liftOver 			https://users.soe.ucsc.edu/~kent/exe/linux/liftOver.gz

../formatting/chain 		https://github.com/ge11232002/CSC/tree/master/WholeGenomeAlignment
../formatting/faToTwoBit	https://github.com/ENCODE-DCC/kentUtils/tree/master/src/utils/faToTwoBit

###Dependencies for Ubuntu16.04
sudo apt-get install git libssl-dev openssl samtools python2.7
sudo rsync -a -P ./src/liftOver /usr/local/bin/
pip install --upgrade pip
pip install pygal
pip install ipython
pip install seaborn

###Python libs used in the pipeline
scipy, seaborn, numpy, matplotlib, os, optparse, shutil, time, datetime, subprocess, sys, pandas, pygal, smtplib

###Scripts located in the ./lncRNA_conservation/bin directory
The main script called RUN.py
Usage: RUN.py [options]

Options:
  -h, --help            show this help message and exit
  -o ORG, --org=ORG     Name of the query organism
  -a ANNOTATION, --annotation=ANNOTATION
                        Annotations source. SRA=1, ensembl=0 (Default: 0)
  --target=TARGET       1 -whole transcriptome(just in case, note that the
                        slncky filtering stage are disable), 2 - lncRNAs only
  --testing=TESTING     # 0 - test saple(first 1000 transcripts), 1 - whole
                        data (Default:1)
  --selfcompare=SELFCOMPARE
                        0 - human vs human, 1 - human vs others (Default: 1)
  --threads=THREADS     Number of threds (Default: 5)
  --minMatch=MINMATCH   minMatch parameter to pass to liftOver. Decreasing
                        this parameter will increase sensitivity of orthology
                        search. (Default: 0.01)
  --pad=PAD             Number of basepairs up- and downstream of lncRNA loci
                        to include in orthology search. Increasing this
                        parameter will increase sensitivity of orthology
                        search. (Default: 100,000)
  --gap_open=GAP_OPEN   Gap open penalty to pass to lastz. Decreasing this
                        parameter will increase sensitivity of orthology
                        search. (Default: 1.0)
  --gap_extend=GAP_EXTEND
                        Gap extend penalty to pass to lastz. Decreasing this
                        parameter will increase sensitivity of orthology
                        search.(Default: 40)
  --min_noncoding=MIN_NONCODING
                        Minimum overlap for reporting transcripts that align
                        to syntenic NONCODING gene. (Default: slncky learns
                        threshold by aligning to shuffled intergenic regions)
  --no_bg=NO_BG         Do not align to shuffled background; report all
                        alignments. Setting this option will greatly reduce
                        slncky runtime.(Default: 0 - align)
  --no_orf=NO_ORF       Do not check for conserved ORFs in aligned noncoding
                        transcripts.(Default: 0 - check)
  --your_email=YOUR_EMAIL
                        Your email for notification reciving
  --server_email=SERVER_EMAIL
                        Server email for notification sending
  --sever_email_pass=SEVER_EMAIL_PASS
                        Server email password for notification sending

