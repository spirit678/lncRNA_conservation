import sys

input = sys.argv[1]
output = sys.argv[2]

f = open(input)
out = open(output, 'w')


c = 0
for line in f:
  if not line[0] == '#':
    line = line.strip().split('\t')
    if line[2] == 'transcript':
      c += 1
      chr = 'chr' + line[0]
      start = line[3]
      end = str(int(line[4]) + 1)
      strand = line[6]
      features = line[8]
      transcript_id = features.split('; ')[1].split(' ')[1].replace('"', '')
      if c <= 10000:
        write = chr + '\t' + start + '\t' + end + '\t' + transcript_id + '\t0\t' + strand + '\n'
        out.write(write)
    
    