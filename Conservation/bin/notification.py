# -*- coding: utf-8 -*-
"""
Created on Wed Aug 10 16:47:55 2016

@author: spirit678
"""
def email_sending():
    import smtplib
    import os
    from email.MIMEMultipart import MIMEMultipart
    from email.MIMEText import MIMEText
    
    import datetime
    txt="Task already done "
    time= str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M"))
    report = txt + time
    
    srv_pass= str(sever_email_pass)
    fromaddr = str(server_email)
    toaddr = str(your_email)
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Server report"
 
    body = report
    msg.attach(MIMEText(body, 'plain'))
 
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, srv_pass)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()


#email_sending(report)
def email_sending_w_log(env_path,lgfold, your_email, server_email, sever_email_pass):
    import smtplib
    import os
    from email.MIMEMultipart import MIMEMultipart
    from email.MIMEText import MIMEText
    from email.MIMEBase import MIMEBase
    from email import encoders

    import datetime
    txt="Task already done "
    time= str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M"))
    import socket
    host = socket.gethostname()
    html = """\
<html>
  <head></head>
  <body>
    <p>{f1}<br>
       Finished at {f2}<br>
       Server hostname is  {f3}<br>
       Project path is {f4}<br>
    </p>
  </body>
</html>
""".format(f1= txt,f2=time,f3=host, f4=env_path)    
    
    
    srv_pass= str(sever_email_pass)
    fromaddr = str(server_email)
    toaddr = str(your_email)
 
    msg = MIMEMultipart()
 
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Server report"
 

 
    msg.attach(MIMEText(html, 'html'))
 
    filename = str("report.tar.gz")
    os.chdir(os.path.join(env_path,"results",lgfold))
    import tarfile
    tar = tarfile.open("report.tar.gz", "w:gz")
    for name in ["experiment.html", "transcripts_pie.svg", "transcripts.svg", "genes_pie.svg", "genes.svg"]:
        tar.add(name)
    tar.close()
    path_1 = os.path.join(env_path, "results", lgfold, filename)    
    #attachment = open("/home/alexey/Conservation_project/Data/logfile-pipe.txt", "rb")
    attachment = open(path_1, "rb")
    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
 
    msg.attach(part)
 
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, srv_pass)
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

"""
from env_choose import env_choose
env_path = str(env_choose(4))
lgfold=str("2017-01-20_16:51__Chimp_all_lncRNA_set")

email_sending_w_log(env_path,lgfold)
"""

