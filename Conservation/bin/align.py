# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import subprocess
import shutil
#####Preparation stage
from env_choose import env_choose
env_path = str(env_choose(2)) #1-laptop, 2-rhesus
org = "CHIMP2.1.4" #['Chimp', 'Baboon', 'Rhesus', 'Gorilla']  
#from copytree import copytre
#work env
path = os.path.join(env_path,"Data", "assemblyDir")
os.mkdir(path, 0777)
###
base_dir = os.path.join(env_path, 'Downloads', 'align', org)
os.chdir(base_dir)
sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
for i in sub_dirs:
        z= os.path.splitext(os.path.basename(i))[0]
        #name = os.path.basename(i)
	print z
        bashCommand = "gunzip " + "-c " + i + " > " + os.path.join(env_path, "Data", "assemblyDir", z)
        os.popen(bashCommand)
        


shutil.copy2(os.path.join(env_path, "Downloads", "align", "Human", "hg38.fa"), os.path.join(env_path, "Data","assemblyDir"))
shutil.copy2(os.path.join(env_path, "src", "primate.matrix"), os.path.join(env_path, "Data","assemblyDir"))
os.chdir(path)
#############Lastz

#############Lastal
###Building index
bashCommand = "lastdb" + " -v" + " -c " + "humdb " + "hg38.fa"
print(bashCommand)
output = subprocess.check_output(["bash", "-c", bashCommand])
###lastal
bashCommand = "lastal" + " -v " + "-p " + "primate.matrix " + "-e30 " + "humdb " + os.path.join(env_path, "Data", "assemblyDir", z) + " > " + org + ".maf"
print(bashCommand)
output = subprocess.check_output(['bash','-c', bashCommand])


## maf to psl
psls = org + ".psl" 
bashCommand = "maf-convert " + "psl " + org + ".maf" + " > " + os.path.join(env_path, "Data", "assemblyDir", psls)
print(bashCommand)
os.popen(bashCommand)



## Join close alignments
shutil.copy2(os.path.join(env_path, "src", "axtChain"), os.path.join(env_path, "Data","assemblyDir"))
shutil.copy2(os.path.join(env_path, "src", "chainMergeSort"), os.path.join(env_path, "Data","assemblyDir"))

bashCommand = "./axtChain" + " -linearGap=medium "  + "-minScore=3000" + "-psl " + psls
print(bashCommand)
os.popen(bashCommand)

## Sort and combine
bashCommand = "./chainMergeSort" + " *.chain "  + "> " + " hg38." + org + ".all.chain" 
print(bashCommand)
os.popen(bashCommand)

