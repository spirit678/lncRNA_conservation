# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 11:59:18 2016

@author: spirit678
"""
#import sys

#input = sys.argv[1]
#output = sys.argv[2]
#input2 = sys.argv[3] # checks for allowed chromosome names


def summary(env_path, lg2, lg3, lg1, target, exp, liftover_set, lift_pad, gap_open, gap_extanded, no_orf, no_bg,  min_noncoding):
    import os
    import csv
    
    os.chdir(os.path.join(env_path, "Data"))
    dict_gene_id = {}
    gtf = open("transcripts.gtf", "r")
    for line in gtf:
      if not line[0] == '#':
        line = line.strip().split('\t')
        if line[2] == 'transcript':
          features = line[8]
          gene_id = features.split('; ')[0].split(' ')[1].replace('"', '')
          transcript_id = features.split('; ')[1].split(' ')[1].replace('"', '')
          if gene_id in dict_gene_id:
            dict_gene_id[gene_id].append(transcript_id)
          else:
            dict_gene_id[gene_id] = [transcript_id]
    
    
    w = csv.writer(open("genes_gtf.csv", "w"))
    for key, val in dict_gene_id.items():
        w.writerow([key, val])
    genes_in_gtf = len(dict_gene_id.keys())
    transcripts_in_gtf = sum(len(v) for v in dict_gene_id.itervalues())
    print("Total number of genes in the gtf file is " + str(genes_in_gtf))
    print("Total number of transcripts in the gtf file is " + str(transcripts_in_gtf))
    
    gtf.close()
    #w.close()
    
    f = open("transcripts_all.bed", "r")
    dict_bed_gene_id ={}
    dict_bed_transcript_id ={}
    for line in f:
        line = line.strip().split('\t')
        transcript_id = line[3]
        id_temp = transcript_id.split(".")[1]
        temp = transcript_id.split(".")[0]
        gene_id = temp + "." + id_temp
        if gene_id not in dict_bed_gene_id:
            dict_bed_gene_id[gene_id] = [transcript_id]
        else:
            dict_bed_gene_id[gene_id].append(transcript_id)
        if transcript_id not in dict_bed_transcript_id:
            dict_bed_transcript_id[transcript_id] = [gene_id]
        else:
            pass     
    
    r = csv.writer(open("genes_bed.csv", "w"))
    for key, val in dict_bed_gene_id.items():
        r.writerow([key, val])
    genes_in_bed = len(dict_bed_gene_id.keys())
    transcripts_in_bed = sum(len(v) for v in dict_bed_gene_id.itervalues())
    print("Total number of genes in the bed file is " + str(genes_in_bed))
    print("Total number of transcripts in the bed file is " + str(transcripts_in_bed))

    f.close()
    #r.close()
        
    ####Orthologs
    dict_ortho_gene_id = {}
    dict_ortho_transcript_id = {}
    ortho = str( lg2 + lg1 + ".orthologs.txt") 
    testing = open(ortho, "r")
    for line in testing:
        if not line[0] == '#':
            line = line.strip().split('\t')
            transcript_id = line[0]
            id_temp = transcript_id.split(".")[1]
            temp = transcript_id.split(".")[0]
            gene_id = temp + "." + id_temp
            if gene_id not in dict_ortho_gene_id:
                dict_ortho_gene_id[gene_id] = [transcript_id]
            else:
                pass
            if transcript_id not in dict_ortho_transcript_id:
                dict_ortho_transcript_id[transcript_id] = [gene_id]
            else:
                pass                
                
    z = csv.writer(open("ortho_genes.csv", "w"))
    for key, val in dict_ortho_gene_id.items():
        z.writerow([key, val])
    
    genes_w_orthologs = len(dict_ortho_gene_id.keys())
    transcripts_w_orthologs = str(len(dict_ortho_transcript_id.keys()))

    print("Total number of genes with orthologs is " + str(genes_w_orthologs))
    print("Total number of transcripts of genes with orthologs is " + str(transcripts_w_orthologs))
  
    testing.close()
    #z.close()
    import matplotlib
    matplotlib.use('Agg')
    import seaborn as sns
    import pandas as pd

    ortho = str( lg2 + lg1 + ".orthologs.top.txt") 
    testing = open(ortho, "r")
    exonID = []
    locusID = []
    indelEx = []
    indelIn = []
    spliceConserved = []
    for line in testing:
        if not line[0] == '#':
            line = line.strip().split('\t')
            transcript_id = line[0]
            try:
                exonID.append(float(line[5]))
                locusID.append(float(line[6]))
                indelEx.append(float(line[7]))
                indelIn.append(float(line[8]))
                spliceConserved.append(float(line[11]))
            except:
                pass
    testing.close()
    print(len(exonID))
    x = pd.Series(exonID, name="exonID all cases(%s)" % len(exonID))
    sns_plot = sns.kdeplot(x, shade=True, cut=0) 
    exonID = [x for x in exonID if x != 0.0]
    #indelEx = [x for x in indelEx if x != 0.0]
    #indelIn = [x for x in indelIn if x != 0.0]
    spliceConserved = [x for x in spliceConserved if x != 0.0]

    print(len(exonID))
    x = pd.Series(exonID, name="exonID non zero value cases(%s)" % len(exonID))
    sns_plot_2 = sns.kdeplot(x, shade=True, cut=0) 
    fig = sns_plot.get_figure()
    fig.savefig("exonID_kde.png")
    fig.clear()
    y = pd.Series(locusID, name="locusID all cases(%s)" % len(locusID))
    plot = sns.kdeplot(y, shade=True, cut=0)

    locusID = [x for x in locusID if x != 0.0] 
    y = pd.Series(locusID, name="locusID all cases(%s)" % len(locusID))
    plot2 = sns.kdeplot(y, shade=True, cut=0)
    fig = plot2.get_figure()
    fig.savefig("locusID_kde.png")
    fig.clear()
    
    print exonID
    import pygal
    from IPython.display import SVG 
    """           
    box_plot = pygal.Box(box_mode="tukey")
    box_plot.title = 'Orthologs top dataset'
    box_plot.add('exon identity', exonID)
    box_plot.add('locus identity', locusID)
    box_plot.add('indel rate exon', indelEx)
    box_plot.add('indel rate intron',  indelIn)
    #box_plot.add('splice site conserved', spliceConserved)
    box_plot.render_to_file('metrics.svg')
    SVG(filename='metrics.svg')
    """
    ################################### Protein coding ########################
    
    if target == 1:
        dict_protein = {}
        dict_protein_transcript={}
        dict_protein_found = {}
        dict_protein_found_transcripts = {}
        f = open("transcript_info.txt", "r")
        for line in f:
            line = line.strip().split('\t')
            transcript_id = line[0]
            id_temp = transcript_id.split(".")[1]
            temp = transcript_id.split(".")[0]
            gene_id = temp + "." + id_temp
            status = line[1]
            function = line[3]
            if "protein_coding" in function and "=" in status:
                if gene_id in dict_protein:
                    dict_protein[gene_id].append(transcript_id)
                else:
                    dict_protein[gene_id] = [transcript_id]
                if transcript_id not in dict_protein_transcript:
                    dict_protein_transcript[transcript_id] = [gene_id]
                else:
                    pass
        rxy = csv.writer(open("genes_protein.csv", "w"))
        for key, val in dict_protein.items():
            rxy.writerow([key, val])
        rxyz = csv.writer(open("transcripts_protein.csv", "w"))
        for key, val in dict_protein_transcript.items():
            rxyz.writerow([key, val])
        genes_protein = len(dict_protein.keys())
        transcripts_protein = sum(len(v) for v in dict_protein.itervalues())
        print("Total number of protein coding genes " + str(genes_protein))
        print("Total number of protein coding transcripts " + str(transcripts_protein))
        print("Total number of protein coding transcripts separete " + str(len(dict_protein_transcript.keys())))
        f.close()
        #rxy.close()

   ######Comparing dictionaries зprot + ortho       
        rx = csv.writer(open("genes_protein_orthologs.csv", "w"))
        count=0
        for key in dict_protein.keys():
            if key in dict_ortho_gene_id.keys():
                rx.writerow([key, val])
                count+=1
                if key not in dict_protein_found:
                    dict_protein_found[key] = [val]
                else:
                    pass
        count1=0
        for key in dict_protein_transcript.keys():
            if key in dict_ortho_transcript_id.keys():
                rx.writerow([key, val])
                count1+=1
                if key not in dict_protein_found_transcripts: ####################changed from dict_protein_found!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    dict_protein_found_transcripts[key] = [val]
                else:
                    pass
        
        #rx.close()
        genes_protein_ortholgs = len(dict_protein_found.keys())    
        print("Total number of protein coding genes with orthologs " + str(genes_protein_ortholgs))
        print(count)
        transcripts_protein_ortholgs = len(dict_protein_found_transcripts.keys())    
        print("Total number of protein coding transcripts with orthologs " + str(transcripts_protein_ortholgs))
        print(count1)
    ######Comparing dictionaries зprot + bed
        dict_gene_prot_bed ={}
        dict_transcript_prot_bed ={}
        count_bed=0
        for key in dict_bed_gene_id.keys():
            if key in dict_protein.keys():
                rx.writerow([key, val])
                count_bed+=1
                if key not in dict_gene_prot_bed.keys():
                    dict_gene_prot_bed[key] = [val]
        count_bed_trans=0
        for key in dict_bed_transcript_id.keys():
            if key in dict_protein_transcript.keys():
                rx.writerow([key, val])
                count_bed_trans+=1
                if key not in dict_transcript_prot_bed.keys():
                    dict_transcript_prot_bed[key] = [val]       
        
        genes_protein_bed = len(dict_gene_prot_bed.keys())    
        print("Total number of protein coding genes in the bed file " + str(genes_protein_bed))
        print(count_bed)
        transcripts_protein_bed = len(dict_transcript_prot_bed.keys())    
        print("Total number of protein coding transcripts in the bed file " + str(transcripts_protein_bed))
        print(count_bed_trans)
        
    ##########################################################################
     #   Total number of prot.codin transcripts in the gtf file is
        dict_prot_transcr_gtf = {}
        dict_protein_found_transcripts_gtf={}
        gtf = open("transcripts.gtf", "r")
        for line in gtf:
          if not line[0] == '#':
            line = line.strip().split('\t')
            if line[2] == 'transcript':
              features = line[8]
              gene_id = features.split('; ')[0].split(' ')[1].replace('"', '')
              transcript_id = features.split('; ')[1].split(' ')[1].replace('"', '')
              if transcript_id not in dict_prot_transcr_gtf:
                dict_prot_transcr_gtf[transcript_id] = [gene_id]

            
        for key in dict_prot_transcr_gtf.keys():
            if key in dict_protein_transcript.keys():
                if key not in dict_protein_found_transcripts_gtf.keys():
                    dict_protein_found_transcripts_gtf[key] = [val]
        protein_found_transcripts_gtf = len(dict_protein_found_transcripts_gtf.keys())    
        print("Total number of protein coding transcripts in the gtf file " + str(protein_found_transcripts_gtf))
        
        #   Total number of prot.codin genes in the gtf file is
        dict_prot_gene_gtf = {}
        dict_protein_found_genes_gtf={}
        gtf = open("transcripts.gtf", "r")
        for line in gtf:
          if not line[0] == '#':
            line = line.strip().split('\t')
            if line[2] == 'transcript':
              features = line[8]
              gene_id = features.split('; ')[0].split(' ')[1].replace('"', '')
              transcript_id = features.split('; ')[1].split(' ')[1].replace('"', '')
              if gene_id not in dict_prot_gene_gtf.keys():
                dict_prot_gene_gtf[gene_id] =[transcript_id]

            
        for key in dict_prot_gene_gtf.keys():
            if key in dict_protein.keys():
                if key not in dict_protein_found_genes_gtf:
                    dict_protein_found_genes_gtf[key] = [val]
        protein_found_genes_gtf = len(dict_protein_found_genes_gtf.keys())    
        print("Total number of protein coding genes in the gtf file " + str(protein_found_genes_gtf))
        
        dict_protein_found_genes_gtf2={}
        for key in dict_gene_id.keys():
            if key in dict_protein.keys():
                if key not in dict_protein_found_genes_gtf2.keys():
                    dict_protein_found_genes_gtf2[key] = [val]
        protein_found_genes_gtf2 = len(dict_protein_found_genes_gtf2.keys())    
        print("Total number of protein coding genes in the gtf file 2 " + str(protein_found_genes_gtf2))        
    ##########################################################################
        genes_w_orthologs = int(genes_w_orthologs)
        genes_in_bed = int(genes_in_bed)  
        genes_in_gtf=int(genes_in_gtf)
        transcripts_w_orthologs = int(transcripts_w_orthologs)
        transcripts_in_bed =int(transcripts_in_bed)
        transcripts_in_gtf=int(transcripts_in_gtf)
        #Stat
        #eleminated = genes_in_gtf - genes_in_bed
        perc_orth = round((float(genes_w_orthologs)*100)/float(genes_in_gtf), 2)
        perc_eliminated = round(((float(genes_in_gtf) - float(genes_in_bed))*100)/float(genes_in_gtf), 2)
        perc_notfound = round(100 - (float(perc_orth) + float(perc_eliminated)), 2)
        
        #transcripts
        perc_ort_trans = round((float(transcripts_w_orthologs)*100)/float(transcripts_in_gtf), 2)
        perc_elim_trans = round(((float(transcripts_in_gtf) - float(transcripts_in_bed))*100)/float(transcripts_in_gtf), 2)
        percent_notfound = round(100 -(float(perc_ort_trans) + float(perc_elim_trans)), 2)
        print "Percents eliminated transcripts"
        print(perc_elim_trans)
        
        eleminates = round(((float(transcripts_in_gtf) - float(transcripts_in_bed))*100)/float(transcripts_in_gtf), 2)
        perc_notfoun = round(((float(transcripts_in_gtf) - (float(transcripts_w_orthologs)+(float(transcripts_in_gtf) - float(transcripts_in_bed))))*100)/float(transcripts_in_gtf), 2)
        else_nonprot = round(((float(transcripts_w_orthologs)-float(transcripts_protein_ortholgs))*100)/float(transcripts_in_gtf), 2)
        prot_trans = round((float(transcripts_protein_ortholgs)*100)/float(transcripts_in_gtf), 2)
        #####
        
        #####
        perc_PCGWO = round(float((float(genes_protein_ortholgs)*100)/float(protein_found_genes_gtf)), 2)
        print( perc_PCGWO)
        perc_elemin_prot_coding = round(((float(protein_found_genes_gtf) - float(genes_protein_bed))*100)/float(protein_found_genes_gtf), 2)
        print(str(perc_elemin_prot_coding))
        perc_PCGNOTO=round(100-(float(perc_PCGWO)+float(perc_elemin_prot_coding)), 2)        
        print(perc_PCGNOTO)
        perc_PCTWO = round(float((float(transcripts_protein_ortholgs)*100)/float(protein_found_transcripts_gtf)), 2)
        print( perc_PCTWO)
        ######Debag####

        ###################################    tsv report  ###################
        report =open("summary.tsv", "w")
        text = "Transcriptome dataset" + '\n'
        report.write(text)
        text = "Query"+ '\t' + lg2 + '\n'
        report.write(text)
        text = exp + '\n'
        report.write(text)
        text = "Genes" + '\t' + str(genes_in_gtf) +'\t' + str(genes_in_bed) + '\t' + str(int(genes_in_gtf)-int(genes_in_bed)) + '\t' + str(genes_w_orthologs) + '\t' + str(perc_orth)  + '\n'
        report.write(text)        
        text = "Transcripts" + '\t' + str(transcripts_in_gtf) +'\t' + str(transcripts_in_bed) + '\t' + str(int(transcripts_in_gtf)-int(transcripts_in_bed)) + '\t' + str(transcripts_w_orthologs) + '\t' + str(perc_ort_trans) + '\n'
        report.write(text)        
        text = "Prot.coding genes" + '\t' + str(protein_found_genes_gtf) +'\t' + str(genes_protein_bed) + '\t' + str(int(protein_found_genes_gtf)-int(genes_protein_bed)) + '\t' + str(genes_protein_ortholgs) + '\t' + str(perc_PCGWO) + '\n'
        report.write(text)
        text = "Prot.coding transcripts" + '\t' + str(protein_found_transcripts_gtf) +'\t' + str(transcripts_protein_bed) + '\t' + str(int(protein_found_transcripts_gtf)-int(transcripts_protein_bed)) + '\t' + str(transcripts_protein_ortholgs) + '\t' + str(perc_PCTWO) + '\n'
        report.write(text)
        report.close        
        ###################################    Visualization  ###################
        import pygal
        from IPython.display import SVG
        chart = pygal.HorizontalBar(show_legend=False, human_readable=True)
        chart.title = 'Gene destribution summary'
        chart.add('Number of protein coding genes with orthologs', int(genes_protein_ortholgs))        
        chart.add('Number of genes with orthologs', int(genes_w_orthologs))
        chart.add('Total number of genes in final bed file', int(genes_in_bed))
        chart.add('Total number of genes in gtf file', int(genes_in_gtf))
        chart.render_to_file('genes.svg')
        SVG(filename='genes.svg')   
        #chart.render_to_png('genes.png')
        
        pie_chart = pygal.Pie(inner_radius=.2)
        pie_chart.title = 'Gene destribution in %'
        pie_chart.add('Genes with orthologs', perc_orth)
        pie_chart.add('Eleminated during preprocessing', perc_eliminated)
        pie_chart.add('Orthologs not found', perc_notfound)
        pie_chart.render_to_file('genes_pie.svg')
        SVG(filename='genes_pie.svg')        
            
        chart = pygal.HorizontalBar(show_legend=False, human_readable=True)
        chart.title = 'Transcripts destribution summary ' 
        chart.add('Number of protein coding transcripts with orthologs', transcripts_protein_ortholgs)
        chart.add('Number of transcripts with orthologs', transcripts_w_orthologs)
        chart.add('Total number of protein coding transcripts in the bed file', transcripts_protein_bed)
        chart.add('Number of transcripts in final bed file', transcripts_in_bed)
        chart.add('Number of transcripts in gtf file', transcripts_in_gtf)
        chart.render_to_file('transcripts.svg')
        SVG(filename='transcripts.svg') 
        #chart.render_to_png('transcripts.png')
        
        from pygal.style import RedBlueStyle
        pie_chart = pygal.Pie(inner_radius=.2, style=RedBlueStyle)
        pie_chart.title = 'Transcripts destribution in %'
        pie_chart.add('Protein coding transcripts with orthologs', prot_trans)
        pie_chart.add('Another transcripts with orthologs', else_nonprot)
        pie_chart.add('Transcripts, eleminated during preprocessing', eleminates)
        pie_chart.add('transcripts where orthologs not found', perc_notfoun)
        pie_chart.render_to_file('trans_pie.svg')
        SVG(filename='trans_pie.svg')
        
        chart = pygal.HorizontalBar(show_legend=False, human_readable=True)
        chart.title = 'Protein coding summary'
        chart.add('Number of protein coding genes with orthologs', int(genes_protein_ortholgs))
        chart.add('Number of protein coding genes in final bed file', int(genes_protein_bed))
        chart.add('Number of protein coding genes in gtf file', int(protein_found_genes_gtf))
        chart.add('Number of protein coding transcripts with orthologs', transcripts_protein_ortholgs)
        chart.add('Number of protein coding transcripts in the bed file', transcripts_protein_bed)
        chart.add('Number of protein coding transcripts in gtf file', int(protein_found_transcripts_gtf))
        chart.render_to_file('prot.svg')
        SVG(filename='prot.svg') 
        #chart.render_to_png('prot.png')
        
        pie_chart = pygal.Pie(inner_radius=.2)
        pie_chart.title = 'Protein coding genes destribution in %'
        pie_chart.add('Protein coding genes with orthologs', perc_PCGWO)
        pie_chart.add('Protein coding genes eleminated', perc_elemin_prot_coding)
        pie_chart.add('Protein coding genes  where orthologs not found', perc_PCGNOTO)
        pie_chart.render_to_file('pro_gen_pie.svg')
        SVG(filename='pro_gen_pie.svg')        
        
        f = open('experiment.html','w')
        message = """<html>
        <head></head>
        <head>
        <style>
        a:link {
            color: blue;
            background-color: transparent;
            text-decoration: none;
        }
        a:visited {
            color: #808080;
            background-color: transparent;
            text-decoration: none;
        }
        a:hover {
            color: red;
            background-color: transparent;
            text-decoration: underline;
        }
        a:active {
            color: yellow;
            background-color: transparent;
            text-decoration: underline;
        }
        table.general {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }
    
        td.general, th.general {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }
    
        tr.general:nth-child(even) {
        background-color: #dddddd;
        }
        </style>
        </head>
        <body>"""
        f.write(message)
        f.close()
        f= open("experiment.html", "a")
        message2 = """
        <h1>Experiment summary</h1>
       
        <h2>Summary statistics</h2>
        <table class="general">
          <tr class="general">
            <td class="general">Experiment</th>
            <td class="general">{f0}</th>
          </tr>
    
          <tr class="general">
            <td class="general">Query species</td>
            <td class="general">{f1}</td>
          </tr>
          <tr class="general">
            <td class="general">Dataset</td>
            <td class="general">Transcriptome</td>
          </tr>
          <tr class="general">
            <td class="general">Total number of genes</td>
            <td class="general">{f2}</td>
          </tr>
          </tr>
            <tr class="general">
            <td class="general">Genes with orthologs</td>
            <td class="general">{f3}</td>
          </tr>
          <tr class="general">
            <td class="general">Genes with orthologs(%)</td>
            <td class="general">{f4}%</td>
          </tr>
            <tr class="general">
            <td class="general">Total number of transcripts</td>
            <td class="general">{f5}</td>
          </tr>
          </tr>
            <tr class="general">
            <td class="general">Transcripts with orthologs</td>
            <td class="general">{f6}</td>
          </tr>
          <tr class="general">
            <td class="general">Transcripts with orthologs(%)</td>
            <td class="general">{f7}%</td>
          </tr>
          <tr class="general">
            <td class="general">Database</th>
            <td class="general"><a href="browse.html" target="_blank">slncky Evolution Browser</a></th>
          </tr>
        </table>""".format(f0=exp,f1=lg2,f2=genes_in_gtf,f3=genes_w_orthologs,f4=perc_orth,f5=transcripts_in_gtf, f6=transcripts_w_orthologs, f7=perc_ort_trans)
        f.write(message2)
        
        message2_1="""
        <h2>Slncky settings</h2>
        <table class="general">
          <tr class="general">
            <td class="general">--minMatch</th>
            <td class="general">{f0}</th>
            <td class="general">Default: 0.1</th>
            <td class="general">-minMatch parameter to pass to liftOver. Decreasing this parameter will increase sensitivity of orthology search.</th>
          </tr>
          <tr class="general">
            <td class="general">--pad</th>
            <td class="general">{f1}</th>
            <td class="general">Default: 50,000</th>
            <td class="general">Number of basepairs up- and downstream of lncRNA loci to include in orthology search. Increasing this parameter will increase sensitivity of orthology search.</th>
          </tr>
          <tr class="general">
            <td class="general">--gap_open</th>
            <td class="general">{f2}</th>
            <td class="general">Default: 1.0</th>
            <td class="general">Gap open penalty to pass to lastz. Decreasing this parameter will increase sensitivity of orthology search</th>
          </tr>
          <tr class="general">
            <td class="general">--gap_extend</th>
            <td class="general">{f3}</th>
            <td class="general">Default: 40</th>
            <td class="general">Gap extend penalty to pass to lastz. Decreasing this parameter will increase sensitivity of orthology search</th>
          </tr>
          <tr class="general">
            <td class="general">ORF check</th>
            <td class="general">{f4}</th>
            <td class="general">Default: 0 - off </th>
            <td class="general">Do not check for conserved ORFs in aligned noncoding transcripts.</th>
          </tr>
          <tr class="general">
            <td class="general">No background</th>
            <td class="general">{f5}</th>
            <td class="general">Default: 0 - off </th>
            <td class="general">Do not align to shuffled background; report all alignments. Setting this option will greatly reduce slncky runtime.</th>
          </tr>
          <tr class="general">
            <td class="general">min_noncoding</th>
            <td class="general">{f6}</th>
            <td class="general">Default: auto </th>
            <td class="general">Minimum overlap for reporting transcripts that align to syntenic NONCODING gene.slncky learns threshold by aligning to shuffled intergenic regions</th>
          </tr>
        </table>""".format(f0=liftover_set,f1=lift_pad,f2=gap_open,f3=gap_extanded, f4=no_orf, f5=no_bg, f6=min_noncoding)
        f.write(message2_1)
        
        message3="""
        <h2>Statistics for gene level</h2>
        <table><tbody>
        <tr><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="genes.svg"><span/></object>
        </td><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="genes_pie.svg"><span/></object>
        </td></tr></tbody></table>
        
        <h2>Statistics for transcript level</h2> 
        <table><tbody>
        <tr><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="transcripts.svg"><span/></object>
        </td><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="trans_pie.svg"><span/></object>
        </td></tr></tbody></table>
        
        <h2>Statistics for protein coding genes and transcripts</h2> 
        <table><tbody>
        <tr><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="prot.svg"><span/></object>
        </td><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="pro_gen_pie.svg"><span/></object>
        </td></tr></tbody></table>
    
        </body>
        </html>"""
        f.write(message3)
        message4="""
        <table class="general">
          <tr class="general">
            <td class="general">Ortologs data</th>
          </tr>
          <tr class="general">
            <td class="general"><a href="{f1}" target="_blank">Top orthologs pairs</a></th>
          </tr>
          <tr class="general">
            <td class="general"><a href="{f0}" target="_blank">All orthologs pairs</a></th>
          </tr>
          <tr class="general">
            <td class="general"><a href="summary.tsv" target="_blank">Summary table</a></th>
          </tr>
        </table>""".format(f0=ortho,f1=str( lg2 + lg1 + ".orthologs.top.txt"))
        f.write(message4)
        f.close()  
    else:
        print("lncRNA dataset")

    ###########################################################################
    ###########################################################################
    ###########################################################################
    if target == 2:
        #Stat
        #eleminated = genes_in_gtf - genes_in_bed
        perc_orth = round((float(genes_w_orthologs)*100)/float(genes_in_gtf), 2)
        perc_eliminated = round(((float(genes_in_gtf) - float(genes_in_bed))*100)/float(genes_in_gtf), 2)
        perc_notfound = round(100 - (float(perc_orth) + float(perc_eliminated)), 2)
        
        #transcripts
        perc_ort_trans = round((float(transcripts_w_orthologs)*100)/float(transcripts_in_gtf), 2)
        perc_elim_trans = round(((float(transcripts_in_gtf) - float(transcripts_in_bed))*100)/float(transcripts_in_gtf), 2)
        percent_notfound = round(100 -(float(perc_ort_trans) + float(perc_elim_trans)), 2)
        print "Percents eliminated transcripts"
        print(perc_elim_trans)
        ###################################    Visualization  ###################
        genes_w_orthologs = int(genes_w_orthologs)
        genes_in_bed = int(genes_in_bed)  
        genes_in_gtf=int(genes_in_gtf)
        transcripts_w_orthologs = int(transcripts_w_orthologs)
        transcripts_in_bed =int(transcripts_in_bed)
        transcripts_in_gtf=int(transcripts_in_gtf)
        report =open("summary.tsv", "w")
        text = "Transcriptome dataset" + '\n'
        report.write(text)
        text = "Query"+ '\t' + lg2 + '\n'
        report.write(text)
        text = exp + '\n'
        report.write(text)
        text = "Genes" + '\t' + str(genes_in_gtf) +'\t' + str(genes_in_bed) + '\t' + str(int(genes_in_gtf)-int(genes_in_bed)) + '\t' + str(genes_w_orthologs) + '\t' + str(perc_orth)  + '\n'
        report.write(text)        
        text = "Transcripts" + '\t' + str(transcripts_in_gtf) +'\t' + str(transcripts_in_bed) + '\t' + str(int(genes_in_gtf)-int(genes_in_bed)) + '\t' + str(transcripts_w_orthologs) + '\t' + str(perc_ort_trans) + '\n'
        report.write(text)
        report.close
        import pygal
        from IPython.display import SVG
        if target == 2:
            chart = pygal.HorizontalBar(show_legend=False, human_readable=True)
            chart.title = 'Gene destribution ' + lg3
            chart.add('Genes with orthologs', genes_w_orthologs)
            chart.add('Preprocessed bed file', genes_in_bed)
            chart.add('gtf file', genes_in_gtf)
            chart.render_to_file('genes.svg')
            SVG(filename='genes.svg')
    
            
            pie_chart = pygal.Pie(inner_radius=.2)
            pie_chart.title = 'Gene destribution in %'
            pie_chart.add('Genes with orthologs', perc_orth)
            pie_chart.add('Genes eleminated during preprocessing', perc_eliminated)
            pie_chart.add('Orthologs not found', perc_notfound)
            pie_chart.render_to_file('genes_pie.svg')
            SVG(filename='genes_pie.svg')
     
            
            chart = pygal.HorizontalBar(show_legend=False, human_readable=True)
            chart.title = 'Transcript destribution ' + lg3
            chart.add('Number of transcripts with orthologs', transcripts_w_orthologs)
            chart.add('Number of transcripts in final bed file', transcripts_in_bed)
            chart.add('Number of transcripts in gtf file', transcripts_in_gtf)
            chart.render_to_file('transcripts.svg')
            SVG(filename='transcripts.svg')
            #chart.render_to_png('transcripts.png')
            
            pie_chart = pygal.Pie(inner_radius=.2)
            pie_chart.title = 'Transcripts destribution in %'
            pie_chart.add('Transcripts with ortholog', perc_ort_trans)
            pie_chart.add('Transcripts eleminated during preprocessing', perc_elim_trans)
            pie_chart.add('Orthologs not found', percent_notfound)
            pie_chart.render_to_file('transcripts_pie.svg')
            SVG(filename='transcripts_pie.svg')
            

        else:
            print("WTF?")         
    
    
        
        f = open('experiment.html','w')
        message = """<html>
        <head></head>
        <head>
        <style>
        a:link {
            color: blue;
            background-color: transparent;
            text-decoration: none;
        }
        a:visited {
            color: #808080;
            background-color: transparent;
            text-decoration: none;
        }
        a:hover {
            color: red;
            background-color: transparent;
            text-decoration: underline;
        }
        a:active {
            color: yellow;
            background-color: transparent;
            text-decoration: underline;
        }
        table.general {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }
    
        td.general, th.general {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }
    
        tr.general:nth-child(even) {
        background-color: #dddddd;
        }
        </style>
        </head>
        <body>"""
        f.write(message)
        f.close()
        f= open("experiment.html", "a")
        message2 = """
        <h1>Experiment summary</h1>
        
        <h2>Summary statistics</h2>
        <table class="general">
          <tr class="general">
            <td class="general">Experiment</th>
            <td class="general">{f0}</th>
          </tr>
    
          <tr class="general">
            <td class="general">Query species</td>
            <td class="general">{f1}</td>
          </tr>
          <tr class="general">
            <td class="general">Dataset</td>
            <td class="general">lncRNAs</td>
          </tr>
          <tr class="general">
            <td class="general">Total number of genes</td>
            <td class="general">{f2}</td>
          </tr>
          </tr>
            <tr class="general">
            <td class="general">Genes with orthologs</td>
            <td class="general">{f3}</td>
          </tr>
          <tr class="general">
            <td class="general">Genes with orthologs(%)</td>
            <td class="general">{f4}%</td>
          </tr>
            <tr class="general">
            <td class="general">Total number of transcripts</td>
            <td class="general">{f5}</td>
          </tr>
          </tr>
            <tr class="general">
            <td class="general">Transcripts with orthologs</td>
            <td class="general">{f6}</td>
          </tr>
          <tr class="general">
            <td class="general">Transcripts with orthologs(%)</td>
            <td class="general">{f7}%</td>
          </tr>
          <tr class="general">
            <td class="general">Database</th>
            <td class="general"><a href="browse.html" target="_blank">slncky Evolution Browser</a></th>
          </tr>
        </table>""".format(f0=exp,f1=lg2,f2=genes_in_gtf,f3=genes_w_orthologs,f4=perc_orth,f5=transcripts_in_gtf, f6=transcripts_w_orthologs, f7=perc_ort_trans)
        f.write(message2)
        message2_1="""
        <h2>Slncky settings</h2>
        <table class="general">
          <tr class="general">
            <td class="general">--minMatch</th>
            <td class="general">{f0}</th>
            <td class="general">Default: 0.1</th>
            <td class="general">-minMatch parameter to pass to liftOver. Decreasing this parameter will increase sensitivity of orthology search.</th>
          </tr>
          <tr class="general">
            <td class="general">--pad</th>
            <td class="general">{f1}</th>
            <td class="general">Default: 50,000</th>
            <td class="general">Number of basepairs up- and downstream of lncRNA loci to include in orthology search. Increasing this parameter will increase sensitivity of orthology search.</th>
          </tr>
          <tr class="general">
            <td class="general">--gap_open</th>
            <td class="general">{f2}</th>
            <td class="general">Default: 1.0</th>
            <td class="general">Gap open penalty to pass to lastz. Decreasing this parameter will increase sensitivity of orthology search</th>
          </tr>
          <tr class="general">
            <td class="general">--gap_extend</th>
            <td class="general">{f3}</th>
            <td class="general">Default: 40</th>
            <td class="general">Gap extend penalty to pass to lastz. Decreasing this parameter will increase sensitivity of orthology search</th>
          </tr>
          <tr class="general">
            <td class="general">ORF check</th>
            <td class="general">{f4}</th>
            <td class="general">Default: 0 - off </th>
            <td class="general">Do not check for conserved ORFs in aligned noncoding transcripts.</th>
          </tr>
          <tr class="general">
            <td class="general">No background</th>
            <td class="general">{f5}</th>
            <td class="general">Default: 0 - off </th>
            <td class="general">Do not align to shuffled background; report all alignments. Setting this option will greatly reduce slncky runtime.</th>
          </tr>
          <tr class="general">
            <td class="general">min_noncoding</th>
            <td class="general">{f6}</th>
            <td class="general">Default:auto </th>
            <td class="general">Minimum overlap for reporting transcripts that align to syntenic NONCODING gene. (Default: slncky learns threshold by aligning to shuffled intergenic regions)</th>
          </tr>
        </table>""".format(f0=liftover_set,f1=lift_pad,f2=gap_open,f3=gap_extanded,f4=no_orf, f5=no_bg, f6=min_noncoding)
        f.write(message2_1)        
        
        message3="""
        <h2>Statistics gene level</h2>
        <table><tbody>
        <tr><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="genes.svg"><span/></object>
        </td><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="genes_pie.svg"><span/></object>
        </td></tr></tbody></table>
        
        <h2>Statistics transcript level</h2> 
        <table><tbody>
        <tr><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="transcripts.svg"><span/></object>
        </td><td><object type="image/svg+xml" width="600" height="500" style="float:left" data="transcripts_pie.svg"><span/></object>
        </td></tr></tbody></table>
        

        <h2>Statistics for transcripts with detected sequence similarity</h2> 
        <table><tbody>
        <tr><td><object type="image/png" width="600" height="500" style="float:left" data="exonID_kde.png"><span/></object>
        </td><td><object type="image/png" width="600" height="500" style="float:left" data="locusID_kde.png"><span/></object>
        </td></tr></tbody></table>

        </body>
        </html>"""
        f.write(message3)
        message4="""
        <table class="general">
          <tr class="general">
            <td class="general">Additional data</th>
          </tr>
          <tr class="general">
            <td class="general"><a href="{f1}" target="_blank">Top orthologs pairs</a></th>
          </tr>
          <tr class="general">
            <td class="general"><a href="{f0}" target="_blank">All orthologs pairs</a></th>
          </tr>
          <tr class="general">
            <td class="general"><a href="summary.tsv" target="_blank">Summary table</a></th>
          </tr>
        </table>""".format(f0=ortho,f1=str( lg2 + lg1 + ".orthologs.top.txt"))
        f.write(message4)
        f.close()  
    ###########################################################################
    ###########################################################################
    ###########################################################################
    elif target == 1:
        pass              
######   Testing
"""
env_path = "/home/alexey/conservation_project/"
lg2 = "Chimp"
lg3 = "transcriptome"# transcriptome or lncRNA_set
lg1 = "_all"
target = int(1)
exp="2016-11-07_12:16__Baboon_all_transcriptome"
liftover_set = 0.01
lift_pad = 80000
gap_open = 1.0
gap_extanded= 40
no_orf=1
summary(env_path, lg2, lg3, lg1, target, exp, liftover_set, lift_pad, gap_open, gap_extanded, no_orf)
"""

