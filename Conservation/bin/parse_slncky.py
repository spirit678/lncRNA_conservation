import sys
"""
test_out2 slncky-master/ transcripts.bed HEK293_transcript_info.txt
out_all slncky-master/ transcripts_all.bed HEK293_transcript_info.txt
prefix = sys.argv[1]
path = sys.argv[2]
slncky_input = sys.argv[3]
transcript_info = sys.argv[4]
"""


def parse_general(prefix, slncky_input, transcript_info, env_path):
	import os
	f1 = open(os.path.join(env_path, "Data", (prefix + '.lncs.bed')))
	f2 = open(os.path.join(env_path, "Data", slncky_input))
	f3 = open(os.path.join(env_path, "Data", transcript_info))
	f4 = open(os.path.join(env_path, "Data", (prefix + '.filtered_info.txt')))


	dict_lncrnas = {}
	for line in f1:
	  line = line.strip().split('\t')
	  transcript = line[3]
	  dict_lncrnas[transcript] = ''

	dict_input = {}
	for line in f2:
	  line = line.strip().split('\t')
	  transcript = line[3]
	  dict_input[transcript] = ''
	  
	dict_data = {}
	for line in f3:
	  line = line.strip().split('\t')
	  transcript = line[0]
	  data = line[1:]
	  dict_data[transcript] = data
	  
	  
	  
	print "Found", len(dict_lncrnas.keys()), "in", len(dict_input.keys()), "input transcripts."

	print "Class codes:"
	print "Input"
	dict_class_codes = {}
	dict_exons = {}
	for transcript in dict_input.keys():
	  data = dict_data[transcript]
	  class_code, nearest_ref, biotype, exons = data
	  if class_code in dict_class_codes:
	    dict_class_codes[class_code] += 1
	  else:
	    dict_class_codes[class_code] = 1
	  if int(exons) > 1:
	    exons = '>1'
	  if exons in dict_exons:
	    dict_exons[exons] += 1
	  else:
	    dict_exons[exons] = 1
	for class_code, count in dict_class_codes.items():
	  print '\t', class_code, str(round(float(count) / len(dict_input.keys()) * 100, 2)) + '%'
	print "Exons"
	for exons, count in dict_exons.items():
	  print '\t', exons, str(round(float(count) / len(dict_input.keys()) * 100, 2)) + '%'
	  
	  
	print "lncRNAs"
	dict_class_codes = {}
	dict_exons = {}
	for transcript in dict_lncrnas.keys():
	  data = dict_data[transcript]
	  class_code, nearest_ref, biotype, exons = data
	  if class_code in dict_class_codes:
	    dict_class_codes[class_code] += 1
	  else:
	    dict_class_codes[class_code] = 1
	  if int(exons) > 1:
	    exons = '>1'
	  if exons in dict_exons:
	    dict_exons[exons] += 1
	  else:
	    dict_exons[exons] = 1
	for class_code, count in dict_class_codes.items():
	  print '\t', class_code, str(round(float(count) / len(dict_lncrnas.keys()) * 100, 2)) + '%'
	print "Exons"
	for exons, count in dict_exons.items():
	  print '\t', exons, str(round(float(count) / len(dict_lncrnas.keys()) * 100, 2)) + '%'


	dict_reasons = {'exonic overlap with coding transcript':0, 'transcript entirely within coding transcript':0, 'Appears to be duplication':0, 'aligns to mm10 coding transcript':0}
	for line in f4:
	  id = line.split('\t')[0]
	  if 'exonic overlap with coding transcript' in line:
	    dict_reasons['exonic overlap with coding transcript'] += 1
	  if 'transcript entirely within coding transcript' in line:
	    dict_reasons['transcript entirely within coding transcript'] += 1
	  if 'Appears to be duplication' in line:
	    dict_reasons['Appears to be duplication'] += 1
	  if 'aligns to mm10 coding transcript' in line:
	    dict_reasons['aligns to mm10 coding transcript'] += 1
	  
	print "Reasons why " + str(len(dict_input.keys()) - len(dict_lncrnas.keys())) + " transcripts were not classified as lncRNA:"
	for reason, count in dict_reasons.items():
	  print reason, count
	    
	    
  
  
