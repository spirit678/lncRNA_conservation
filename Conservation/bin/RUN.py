#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 15 12:39:49 2016

@author: spirit678
"""

import os
import optparse
import shutil
import time
import datetime
import subprocess
import sys
from copytree import copytree
###################################
if not sys.version_info[:2] < (3, 0):
    print("Warning: Python 3 detected please use Python 2.x version")
    print(sys.version_info)
else:
    pass


def is_tool(name):
    try:
        devnull = open(os.devnull)
        subprocess.Popen([name], stdout=devnull, stderr=devnull).communicate()
    except OSError as e:
        if e.errno == os.errno.ENOENT:
            print(str(name + "not found in your system"))
            print("Samtools (http://samtools.sourceforge.net/) and liftOver from kentUtils (https://github.com/ENCODE-DCC/kentUtils) should be installed in your system!")
            sys.exit()
            return False
    return True

is_tool("liftOver")
is_tool("samtools")

parser = optparse.OptionParser()

parser.add_option('--org', "-o", action="store", type="str", help="Name of the query organism")
parser.add_option('--annotation', "-a", action="store", type="int", default="0", help="Annotations source. ncbi=1, ensembl=0 (Default: 0)")
parser.add_option('--target', action="store", type="int", default="2", help="1 -whole transcriptome(just in case, note that the slncky filtering stage are disable), 2 - lncRNAs only")
parser.add_option('--testing', action="store", type="int", default="1", help="# 0 - test saple(first 1000 transcripts), 1 - whole data (Default:1)")
parser.add_option('--selfcompare', action="store", type="int", default="1", help="0 - human vs human, 1 - human vs others (Default: 1)")
parser.add_option('--threads', action="store", type="int", default="5", help="Number of threds (Default: 5)")


parser.add_option('--minMatch', action="store", type="float", default="0.01", help="minMatch parameter to pass to liftOver. Decreasing this parameter will increase sensitivity of orthology search. (Default: 0.01)")
parser.add_option('--pad', action="store", type="int", default="100000", help="Number of basepairs up- and downstream of lncRNA loci to include in orthology search. Increasing this parameter will increase sensitivity of orthology search. (Default: 100,000)")
parser.add_option('--gap_open', action="store", type="float",default="1.0", help="Gap open penalty to pass to lastz. Decreasing this parameter will increase sensitivity of orthology search. (Default: 1.0)")
parser.add_option('--gap_extend', action="store", type="int", default="40", help="Gap extend penalty to pass to lastz. Decreasing this parameter will increase sensitivity of orthology search.(Default: 40)")
parser.add_option('--min_noncoding', action="store", type="int", default="0", help="Minimum overlap for reporting transcripts that align to syntenic NONCODING gene. (Default: slncky learns threshold by aligning to shuffled intergenic regions)")
parser.add_option('--no_bg', action="store", type="int", default="0", help="Do not align to shuffled background; report all alignments. Setting this option will greatly reduce slncky runtime.(Default: 0 - align)")
parser.add_option('--no_orf', action="store", type="int", default="0", help="Do not check for conserved ORFs in aligned noncoding transcripts.(Default: 0 - check)")


parser.add_option('--your_email', action="store", type="str", default="NONE", help="Your email for notification reciving")
parser.add_option('--server_email', action="store", type="str", default="NONE", help="Server email for notification sending")
parser.add_option('--sever_email_pass', action="store", type="str", default="NONE", help="Server email password for notification sending")

options, args = parser.parse_args()

if len(sys.argv[1:]) == 0:
    print("At least --org or -o option should be defined")
    parser.print_help()
    sys.exit(1)

####First run
from env_choose import env_choose
env_path = str(env_choose(4)) #1-laptop, 2-rhesus, 3-bonobo, 4-unified
####
if not os.path.exists(os.path.join(env_path, 'Downloads')):
    print("During first RUN test data should be download.")
    print("Samtools (http://samtools.sourceforge.net/) and liftOver from kentUtils (https://github.com/ENCODE-DCC/kentUtils) should be installed in your system!")
    print("lastz and bedtools are provided as binaries and there is no need to install them")
    print ('###########################################')
    
    bashCommand = "wget -P ../ http://rhesus.amu.edu.pl/alexey/Downloads.tar.gz"
    output = subprocess.check_output(['bash','-c', bashCommand])
    print(output)
    bashCommand = "tar -zxvf ../Downloads.tar.gz -C ../"
    output = subprocess.check_output(['bash','-c', bashCommand])
    print(output)
    bashCommand = "rm ../Downloads.tar.gz"
    os.popen(bashCommand)
else:
    pass
if not os.path.exists(os.path.join(env_path, 'Data')):
    os.mkdir(os.path.join(env_path, 'Data'), 0o777)

else:
    shutil.rmtree(os.path.join(env_path, "Data"))
    os.mkdir(os.path.join(env_path, 'Data'), 0o777)
if not os.path.exists(os.path.join(env_path, 'results')):
    os.mkdir(os.path.join(env_path, 'results'), 0o777)
else:
    pass
   
print ('###########################################')
print ('Query organism is %s' % (options.org))
if options.annotation == 0:
    print("Annotations type: ensembl")
elif options.annotation == 1:
    print("Annotations type: SRA")
if options.selfcompare == 0:
    print("Warning: selfcompare option enabled, query is the same as target, e.g. human")
else:
    pass
if options.testing == 0:
    print("Warning: test saple(first 1000 transcripts)")
else:
    pass
if options.target == 1:
    print("Warning: whole transcriptome used as a source of lncRNAs")
else:
    pass
print ('#### Orthology Search Options ##############')
if options.min_noncoding != 0:
    print("Warning not default: Minimum overlap for reporting transcripts that align to syntenic NONCODING gene.")
    print ('no_orf : %s' % (options.no_orf))
else:
    print ('min_noncoding : slncky learns threshold by aligning to shuffled intergenic regions')
if options.no_orf != 0:
    print("Warning: Do not align to shuffled background; report all alignments")
    print ('no_orf : %s' % (options.no_orf))
else:
    pass
if options.no_bg != 0:
    print("Warning: Do not check for conserved ORFs in aligned noncoding transcripts")
    print ('no_bg : %s' % (options.no_bg))
else:
    pass
print ('#### Advanced orthology search options #####')
print ('threads : %s' % (options.threads))
print ('minMatch : %s' % (options.minMatch))
print ('pad  : %s' % (options.pad))
print ('gap_open   : %s' % (options.gap_open))
print ('gap_extend : %s' % (options.gap_extend))
print ('#### Email notification ###################')
if options.your_email != "NONE":
    if options.server_email != "NONE":
        if options.sever_email_pass != "NONE":
            print ('Your email : %s' % (options.your_email))
            print ('Server email : %s' % (options.server_email))
            print ('Server email password : %s' % (options.sever_email_pass))
        else:
            pass
    else:
        pass 
else:
    print ('Email notification disabled, in case you need it use --help')     
print ('###########################################')



#Slnckey config should be here
liftover_set = options.minMatch
lift_pad = options.pad
gap_open = options.gap_open
gap_extanded= options.gap_extend

no_orf = options.no_orf
no_bg = options.no_bg  #1 --no_orf ON, 0 Off)
min_noncoding = options.min_noncoding #1 --no_orf ON, 0 Off)

threads= options.threads
###################################
#General config
print ('###########################################')
env = int(options.testing) # 0 - test saple, 1 - whole data
hum_vs_all = int(options.selfcompare) # 0 - human, 1 - others
#####!!!!org works only when hum_vs_all == 1
org = options.org #['Chimp', 'Baboon', 'Rhesus', 'Gorilla', 'Lemur', 'Orangutan', "MacacaFascicularis"
ncbi = options.annotation # 0 -off, 1 -on
target = options.target#1 -whole transcriptome, 2 - lncRNAs only
#####Needas for logging
folder=str(datetime.datetime.now().strftime("%Y-%m-%d_%H:%M") + "_")
###################################
from general_no_parse import pipeline
pipeline(env_path, env, hum_vs_all, org, folder, target, liftover_set, lift_pad, gap_open, gap_extanded, no_orf, no_bg, min_noncoding, ncbi, threads)

##################################report#######################
from notification import email_sending_w_log
if env == 0:
    lg1="_test"
else:
    lg1="_all"
if hum_vs_all == 0:
    lg2="Human"
else:
    lg2=org
if target== 1:
	lg3="_transcriptome"
elif target == 2:
	lg3= "_lncRNA_set"
else:
	print("error!")
lg= str(folder + "_" + lg2 + lg1 + lg3 + ".log")
lgfold=str(folder + "_" + lg2 + lg1 + lg3)
ortho = str( lg2 + lg1 + "orthologs.txt")
exp=str(folder + "_" + lg2 + lg1 + lg3 + ".log")
#####Summary report#########################################
from summary import summary
summary(env_path, lg2, lg3, lg1, target, exp, liftover_set, lift_pad, gap_open, gap_extanded, no_orf, no_bg, min_noncoding)
#####Moving to results#########################################

folder = str(folder + "_" + lg2 + lg1 + lg3)
base_dir = os.path.join(env_path, 'Data')
path = os.path.join(env_path,"results", folder)
os.mkdir(path, 0o777)
print (path)
#out_test = org + "_test"
if hum_vs_all == 0:
    if env == 0:
        out_all = "Human_test"
    else:
        out_all = "Human_all"
if hum_vs_all == 1:
    if env == 0:
        out_all = org + "_test"
    else:
        out_all = org + "_all"
browser = out_all + ".EvolutionBrowser"  
    
for i in os.listdir(os.path.join(env_path, "Data")):
    if i.startswith(out_all):
            inp_file = os.path.basename(i)
            print(inp_file)
            if os.path.isdir(os.path.join(env_path, "Data",i)):
                print(i)                
                pass
            else:    
                shutil.copy2(os.path.join(base_dir, inp_file), path)

shutil.copy2(os.path.join(base_dir, "log_slncky"), path)
copytree(os.path.join(env_path, "Data", browser ), path)
shutil.copy2(os.path.join(base_dir, "transcripts_all.bed"), path)
shutil.copy2(os.path.join(base_dir, "experiment.html"), path)
shutil.copy2(os.path.join(base_dir, "summary.tsv"), path) 
#shutil.copy2(os.path.join(base_dir, "browse.html"), path)
if target== 1:
    shutil.copy2(os.path.join(base_dir, "genes.svg"), path)
    shutil.copy2(os.path.join(base_dir, "genes_pie.svg"), path)
    shutil.copy2(os.path.join(base_dir, "transcripts.svg"), path)
    shutil.copy2(os.path.join(base_dir, "trans_pie.svg"), path)
    shutil.copy2(os.path.join(base_dir, "prot.svg"), path)
    shutil.copy2(os.path.join(base_dir, "pro_gen_pie.svg"), path)
    shutil.copy2(os.path.join(base_dir, "exonID_kde.png"), path)
    shutil.copy2(os.path.join(base_dir, "locusID_kde.png"), path)
elif target == 2:
    shutil.copy2(os.path.join(base_dir, "genes.svg"), path)
    shutil.copy2(os.path.join(base_dir, "genes_pie.svg"), path)
    shutil.copy2(os.path.join(base_dir, "transcripts.svg"), path)
    shutil.copy2(os.path.join(base_dir, "transcripts_pie.svg"), path)
    shutil.copy2(os.path.join(base_dir, "exonID_kde.png"), path)
    shutil.copy2(os.path.join(base_dir, "locusID_kde.png"), path)
else:
	print("error!")

managing_results = "rm -rf " + env_path + "Data/"
os.popen(managing_results)
os.mkdir(os.path.join(env_path, "Data"), 0o777)

print("Task finish")
##############################################################

time.sleep(10)
your_email = options.your_email
server_email = options.server_email
sever_email_pass = options.sever_email_pass
if your_email !="NONE":
    if server_email !="NONE":
        if sever_email_pass !="NONE":
            email_sending_w_log(env_path,lgfold, your_email, server_email, sever_email_pass)
        else:
            pass
    else:
        pass
else:
    pass
from generate import generate
generate(org)
