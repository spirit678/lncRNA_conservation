# -*- coding: utf-8 -*-
"""
Created on Sat Nov 26 13:48:56 2016

@author: spirit678
"""
def generate():
	import os
	from env_choose import env_choose
	env_path = str(env_choose(4))
	"""

	"""
	os.chdir(os.path.join(env_path, "results"))
	print(env_path)
	f = open('Summary.html','w')
	message = """<html>
	<head></head>
	<head>
	<style>
	a:link {
	    color:  #212f3c;
	    background-color: transparent;
	    text-decoration: none;
	}
	a:visited {
	    color: #808080;
	    background-color: transparent;
	    text-decoration: none;
	}
	a:hover {
	    color: red;
	    background-color: transparent;
	    text-decoration: underline;
	}
	a:active {
	    color: yellow;
	    background-color: transparent;
	    text-decoration: underline;
	}
	table.general {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
	}
	    
	td.general, th.general {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
	}
	    
	tr.general:nth-child(even) {
	background-color: #dddddd;
	}
	</style>
	</head>
	<body style="background-color: #fbfcfc;">
	<h2>Experiments list</h2>
	<table class="general">
	    <tr class="general">
		<td class="general">Organism</th>        
		<td class="general">Transcriptome total genes</th>
		<td class="general">Genes with orthologs</th>
		<td class="general">Genes with orthologs percent</th>
		<td class="general">Protein coding genes total</th>
		<td class="general">Protein coding genes with orthologs</th>
		<td class="general">Protein_coding genes_with orthologs percent</th>
		<td class="general">lncRNAs total genes</th>
		<td class="general">lncRNAs genes with orthologs</th>
		<td class="general">lncRNAs genes with orthologs percent</th>
	    </tr>"""
	f.write(message)
	f.close()

	#org_list = ['Chimp', 'Baboon', 'Rhesus', 'Gorilla','Marmoset','GreenMonkey','Orangutan', 'CrabEating', 'Tarsier', 'Lemur', 'Bonobo']
	org_list =['Chimp','Baboon','Rhesus', 'Gorilla','Marmoset','Orangutan', "Atys"]
	os.chdir(os.path.join(env_path, "results"))


	dict_transcriptome_gene_ortholgs = {}
	exp=[]
	exp_lnc=[]
	transcriptome_gene_ortholgs=[]
	transcriptome_gene_ortholgs_perc = []

	transcriptome_transcript_ortholgs = []
	transcriptome_transcript_ortholgs_perc = []

	transcriptome_prot_gene_ortholgs = []
	transcriptome_prot_gene_ortholgs_perc = []

	transcriptome_prot_transcript_ortholgs = []
	transcriptome_prot_transcript_ortholgs_perc = []

	lncTotalGenes=[]
	lncTotalTrans=[]

	lncOrth=[]
	lncOrthPercent=[]

	lncOrthTrans=[]
	lncOrthTransPercent=[]

	aList=[]

	monkey=[]


	for j in org_list:
	    for i in os.listdir(os.path.join(env_path, "results")):
		if j in i:
		    monkey.append(j)
		    if i.endswith("all_transcriptome"):
		        print(i)
		        experiment = os.path.basename(i)
		        summary= open(os.path.join(env_path, "results", experiment, "summary.tsv"), "r")
		        summary.readline(3)
		        link= experiment + "/experiment.html"
		        for line in summary:
		            line = line.strip().split('\t')
		            if line[0] == 'Genes':
		                genes_gtf = line[1]
		                genes_bed = line[2]
		                eliminated = line[3]
		                genes_w_orthologs = line[4]
		                genes_w_percents = line[5]
		                if experiment not in dict_transcriptome_gene_ortholgs:
		                    dict_transcriptome_gene_ortholgs[experiment] = [genes_w_orthologs]
		                    exp.append(experiment)
		                    transcriptome_gene_ortholgs.append(genes_w_orthologs)
		                    transcriptome_gene_ortholgs_perc.append(genes_w_percents)
		            if line[0] == 'Transcripts':
		                transcript_gtf = line[1]
		                transcript_bed = line[2]
		                transcript_eliminated = line[3]
		                transcript_w_orthologs = line[4]
		                transcript_w_percents = line[5]
		                transcriptome_transcript_ortholgs.append(transcript_w_orthologs)
		                transcriptome_transcript_ortholgs_perc.append(transcript_w_percents)
		            if line[0] == 'Prot.coding genes':
		                prot_genes_gtf = line[1]
		                prot_genes_bed = line[2]
		                prot_eliminated = line[3]
		                prot_genes_w_orthologs = line[4]
		                prot_genes_w_percents = line[5]
		                transcriptome_prot_gene_ortholgs.append(prot_genes_w_orthologs)
		                transcriptome_prot_gene_ortholgs_perc.append(prot_genes_w_percents)
		            if line[0] == 'Prot.coding transcripts':
		                prot_transcript_gtf = line[1]
		                prot_transcript_bed = line[2]
		                prot_transcript_eliminated = line[3]
		                prot_transcript_w_orthologs = line[4]
		                prot_transcript_w_percents = line[5]
		                transcriptome_prot_transcript_ortholgs.append(prot_transcript_w_orthologs)
		                transcriptome_prot_transcript_ortholgs_perc.append(prot_transcript_w_percents)
		        f= open("Summary.html", "a")
		        message2_1="""
	    <tr class="general">
		<td class="general"><a href="{link}" target="_blank">{exp}</a></th>
		<td class="general">{f0}</th>
		<td class="general">{f1}</th>
		<td class="general">{f2}</th>
		<td class="general">{f3}</th>
		<td class="general">{f4}</th>
		<td class="general">{f5}</th>
		<td class="general">-</th>
		<td class="general">-</th>
		<td class="general">-</th>
	    </tr>
		         """.format(link=link,exp=experiment,f0=genes_gtf,f1=genes_w_orthologs,f2=genes_w_percents,f3=prot_genes_gtf, f4=prot_genes_w_orthologs, f5=prot_genes_w_percents)
		        f.write(message2_1)
		    if i.endswith("all_lncRNA_set"):
		        experiment = os.path.basename(i)
		        link= experiment + "/experiment.html"
		        summary= open(os.path.join(env_path, "results", experiment, "summary.tsv"), "r")
		        summary.readline(3)
		        for line in summary:
		            line = line.strip().split('\t')
		            if line[0] == 'Genes':
		                genes_gtf = line[1]
		                genes_bed = line[2]
		                eliminated = line[3]
		                genes_w_orthologs = line[4]
		                genes_w_percents = line[5]
		                if experiment not in dict_transcriptome_gene_ortholgs:
		                    dict_transcriptome_gene_ortholgs[experiment] = [genes_w_orthologs]
		                    exp_lnc.append(experiment)
		                    lncOrth.append(genes_w_orthologs)
		                    lncOrthPercent.append(genes_w_percents)
		            if line[0] == 'Transcripts':
		                transcript_gtf = line[1]
		                transcript_bed = line[2]
		                transcript_eliminated = line[3]
		                transcript_w_orthologs = line[4]
		                transcript_w_percents = line[5]
		                lncOrthTrans.append(transcript_w_orthologs)
		                lncOrthTransPercent.append(transcript_w_percents)
		        f= open("Summary.html", "a")
		        message2_2="""

		          <tr class="general">
		            <td class="general"><a href="{link}" target="_blank">{exp}</a></th>
		            <td class="general">-</th>
		            <td class="general">-</th>
		            <td class="general">-</th>
		            <td class="general">-</th>
		            <td class="general">-</th>
		            <td class="general">-</th>
		            <td class="general">{f0}</th>
		            <td class="general">{f1}</th>
		            <td class="general">{f2}</th>
		          </tr>
		         """.format(link=link,exp=experiment,f0=genes_gtf,f1=genes_w_orthologs,f2=genes_w_percents)
		        f.write(message2_2)
	message3="""
	</table>"""
	f.write(message3)
	os.chdir(os.path.join(env_path, "results"))






generate()
"""
########Visualization
import pygal
from IPython.display import SVG 
	   
line_chart = pygal.Line(x_label_rotation=30)
line_chart.title = 'Whole transcriptome summary (in % from source dataset)'
line_chart.x_labels = map(str, exp)
line_chart.y_labels = range(0, 100, 10)
line_chart.add('Genes with orthologs', map(float, transcriptome_gene_ortholgs_perc))
line_chart.add('Transcripts with orthologs', map(float, transcriptome_transcript_ortholgs_perc))
line_chart.add('Protein Genes with orthologs', map(float, transcriptome_prot_gene_ortholgs_perc))
line_chart.add('Protein transcripts with orthologs', map(float, transcriptome_prot_transcript_ortholgs_perc))


line_chart.render_to_file('transcriptome.svg')
SVG(filename='transcriptome.svg')  

line_chart = pygal.Line(x_label_rotation=30)
line_chart.title = 'lncRNAs summary (in % from source dataset)'
line_chart.x_labels = map(str, exp_lnc)
line_chart.y_labels = range(0, 100, 10)
line_chart.add('lncRNAs genes with orthologs', map(float, lncOrthPercent))
line_chart.add('lncRNAs transcripts with orthologs', map(float, lncOrthTransPercent))

line_chart.render_to_file('lncRNAs.svg')
SVG(filename='lncRNAs.svg')  
"""

