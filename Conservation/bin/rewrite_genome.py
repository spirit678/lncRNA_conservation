def genome_re(env_path, org):
    import os
    os.chdir(os.path.join(env_path, "Data", "annotations"))
    for file in os.listdir(os.path.join(env_path, "Data", "annotations")):
        if file.endswith(".fa"):
            if file.startswith("hg19") or file.startswith("hg38") or file.startswith("mm10") or file.startswith("mm9"):
                pass
            else:
                inp_file = os.path.basename(file)
                print(inp_file)
     
    f = open(os.path.join(env_path, "Data", "annotations", str(inp_file)), "r")
    out = open(os.path.join(env_path, "Data", "annotations", str(org + ".fa")), "w")
    


    for line in f:
      if line[0] == '>':
        id = line[1:].split(' ')[0].strip()
        if len(id) < 3 and id != 'MT':
          id = 'chr' + id
        line = '>' + id + '\n'
      out.write(line)
    out.close
    f.close  

  
