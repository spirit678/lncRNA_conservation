
def annotations(org,env_path):
    import os
    base_dir = os.path.join(env_path, 'Data', 'annotations')
    sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
    for file in sub_dirs:
        if file.endswith(".gtf"):
                inp_file = os.path.basename(file)
                print(inp_file)
                inp_fi = os.path.join(env_path, "Data", "annotations", str(inp_file))

    
    species = org
    os.chdir(os.path.join(env_path, "Data", "annotations"))
    #inp_fi = os.path.join(env_path, "Data", "annotations", str(inp_file))
    print(inp_fi)    
    f = open(inp_fi, "r")

    out1 = open(os.path.join(env_path, 'Data', 'annotations', (species + '_coding.bed')), 'w') # protein_coding
    out2 = open(os.path.join(env_path, 'Data', 'annotations', (species + '_snoRNAs.bed')), 'w') # snoRNAs
    out3 = open(os.path.join(env_path, 'Data', 'annotations', (species + '_miRNAs.bed')), 'w') # miRNAs
    out4 = open(os.path.join(env_path, 'Data', 'annotations', (species + '_gene_names.txt')), 'w') # gene_names
    out5 = open(os.path.join(env_path, 'Data', 'annotations', (species + '_lncRNAs.bed')), 'w') # lncRNAs (including pseudogenes)
    
  
    lncrna_biotypes = ['lncRNA', 'lncRNAs']


    dict_exon_starts = {}
    dict_exon_lengths = {}
    dict_data = {}
    for line in f:
      if not line[0] == '#':
        line = line.strip().split('\t')
        start = int(line[3]) - 1
        end = int(line[4])
        strand = line[6]
        features = line[8]
        if line[2] == 'exon':
          features = features.split('; ')
          for elem in features:
            if "transcript_id" in elem:
              transcript_id = elem.split(' ')[1].replace('"', '')
          if transcript_id in dict_exon_starts:
            dict_exon_starts[transcript_id].append(start)
          else:
            dict_exon_starts[transcript_id] = [start]
          length = abs(end-start)
          if transcript_id in dict_exon_lengths:
            dict_exon_lengths[transcript_id].append(length)
          else:
            dict_exon_lengths[transcript_id] = [length]
        if line[2] == 'transcript':
          features = features.split('; ')
          gene_id, transcript_id, gene_name, transcript_biotype = 'none', 'none', 'none', 'none'
          for elem in features:
            if "gene_id" in elem:
              gene_id = elem.split(' ')[1].replace('"', '')
            if "transcript_id" in elem:
              transcript_id = elem.split(' ')[1].replace('"', '')          
            if "gene_name" in elem:
              gene_name = elem.split(' ')[1].replace('"', '')  
            if "transcript_biotype" in elem:
              transcript_biotype = elem.split(' ')[1].replace('"', '').replace(';', '').strip()
          dict_data[transcript_id] = (gene_id, gene_name, transcript_biotype)
            
            
    f.close()
    f = open(inp_fi, "r")
    
    
    for line in f:
      if not line[0] == '#':
        line = line.strip().split('\t')
        if line[2] == 'transcript':
          if len(line[0]) < 3 and line[0] != 'MT':
            chr = 'chr' + line[0]
          else:
            chr = line[0]
          start = str(int(line[3]) - 1)
          end = str(int(line[4]))
          strand = line[6]
          features = line[8].split('; ')
          for elem in features:
            if "transcript_id" in elem:
              transcript_id = elem.split(' ')[1].replace('"', '')
          exon_starts = dict_exon_starts[transcript_id]
          exon_lengths = dict_exon_lengths[transcript_id]
          s = int(start)
          exon_starts = [elem - s for elem in exon_starts]
          exon_lengths = [str(elem) for elem in exon_lengths]
          exon_starts = [str(elem) for elem in exon_starts]
          block_count = len(exon_starts)
          exon_starts = ','.join(exon_starts)
          exon_lengths = ','.join(exon_lengths)
          write = chr + '\t' + start + '\t' + end + '\t' + transcript_id + '\t0\t' + strand + '\t' + str(start) + '\t' + str(end) + '\t0\t' + str(block_count) + '\t' + exon_lengths + '\t' + exon_starts + '\n'
          data = dict_data[transcript_id]
          gene_id, gene_name, transcript_biotype = data
          if transcript_biotype == 'protein_coding':
            out1.write(write)
          if transcript_biotype == 'snoRNA':
            out2.write(write)
          if transcript_biotype == 'miRNA':
            out3.write(write)
          out4.write(transcript_id + '\t' + gene_name + '\n')
          if transcript_biotype in lncrna_biotypes:
            out5.write(write)      
    out1.close()
    out2.close()  
    out3.close()
    out4.close() 
    out5.close()
          
            
#annotations("Chimp", "/home/spirit678/Work/Conservation/")