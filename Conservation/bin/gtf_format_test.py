# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 14:01:01 2016

@author: spirit678
"""

def gtf_format_test_1000(inpu, outpu, env_path):
    import os
    f = open(os.path.join(env_path, "Data", inpu))
    out = open(os.path.join(env_path, "Data", outpu), 'w')
    
    dict_exon_starts = {}
    dict_exon_lengths = {}
    for line in f:
      if not line[0] == '#':
        line = line.strip().split('\t')
        if line[2] == 'exon':
          start = int(line[3]) - 1
          end = int(line[4])
          strand = line[6]
          features = line[8]
          transcript_id = features.split('; ')[1].split(' ')[1].replace('"', '')
          #if strand == '-':
          #  start, end = end, start
          if transcript_id in dict_exon_starts:
            dict_exon_starts[transcript_id].append(start)
          else:
            dict_exon_starts[transcript_id] = [start]
          length = abs(end-start)
          if transcript_id in dict_exon_lengths:
            dict_exon_lengths[transcript_id].append(length)
          else:
            dict_exon_lengths[transcript_id] = [length]
            
            
    f.close()
    f = open(os.path.join(env_path, "Data", inpu))
    
    c = 0
    for line in f:
      if not line[0] == '#':
        line = line.strip().split('\t')
        if line[2] == 'transcript':
          c += 1
          chr = 'chr' + line[0]
          start = str(int(line[3]) - 1)
          end = str(int(line[4]))
          strand = line[6]
          features = line[8]
          transcript_id = features.split('; ')[1].split(' ')[1].replace('"', '')
          exon_starts = dict_exon_starts[transcript_id]
          exon_lengths = dict_exon_lengths[transcript_id]
       #   if strand == '+':
          s = int(start)
          exon_starts = [elem - s for elem in exon_starts]
       #   else:
       #     s = int(end)
       #     exon_starts = [s - elem for elem in exon_starts]
       #     exon_starts.reverse()
       #     exon_lengths.reverse()
          exon_lengths = [str(elem) for elem in exon_lengths]
          exon_starts = [str(elem) for elem in exon_starts]
          block_count = len(exon_starts)
          exon_starts = ','.join(exon_starts)
          exon_lengths = ','.join(exon_lengths)
          if c <= 10000:
            write = chr + '\t' + start + '\t' + end + '\t' + transcript_id + '\t0\t' + strand + '\t' + str(start) + '\t' + str(end) + '\t0\t' + str(block_count) + '\t' + exon_lengths + '\t' + exon_starts + '\n'
            out.write(write)
