# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 12:57:11 2016

@author: spirit678
"""

def make_cfg(env_path, org):
    import os
    #managing_results = "rm -rf " + env_path + "Data/" + "annotations.config"
    #os.popen(managing_results)
    os.chdir(os.path.join(env_path, "Data"))
    f = open(os.path.join(env_path, "Data", "annotations.config"), "a")
    text= '\n'
    f.write(text)
    text= ">human" + '\n'
    f.write(text)
    text= "CODING=annotations/hg38.gencode.v23.basic.coding.bed" + '\n'
    f.write(text)
    text= "CODING=annotations/hg38.gencode.v23.pseudo.bed" + '\n'
    f.write(text)
    text= "CODING=annotations/hg38.refseq.coding.bed" + '\n'
    f.write(text)
    text= "ORTHOLOG=" + org +'\n'
    f.write(text)
    for file in os.listdir(os.path.join(env_path, "Data", "annotations")):
        if file.endswith(".gz") and file.startswith("hg38."):
            inp_file = file
            text= "LIFTOVER=annotations/" + inp_file + '\n'
            f.write(text)
    text= "GENOME_FA=annotations/hg38.fa" + '\n'
    f.write(text)
    text= "DUPS=annotations/hg38.zfps.fa" + '\n'
    f.write(text)    
    text= "DUPS=annotations/hg38.olfs.fa" + '\n'
    f.write(text)  
    text= "NONCODING=annotations/hg38.gencode.v23.lncRNA.bed" + '\n'
    f.write(text)  
    text= "GENESYMBOL=annotations/hg38.geneSymbolMap.txt" + '\n'
    f.write(text)  
    text= "MIRNA=annotations/hg38.miRNAs.bed" + '\n'
    f.write(text)  
    text= "SNORNA=annotations/hg38.snoRNAs.bed" + '\n'
    f.write(text)  
    text= '\n'
    f.write(text)
    text= ">" + org + '\n'
    f.write(text) 
    text= "CODING=annotations/" + org + "_coding.bed" + '\n'
    f.write(text) 
    text= "ORTHOLOG=human" + '\n'
    f.write(text) 
    text= "GENOME_FA=annotations/" + org + ".fa" + '\n'
    f.write(text) 
    text= "NONCODING=annotations/" + org + "_lncRNAs.bed" + '\n'
    f.write(text) 
    text= "GENESYMBOL=annotations/" + org + "_gene_names.txt" + '\n'
    f.write(text)     
    text= "MIRNA=annotations/" + org + "_miRNAs.bed" + '\n'
    f.write(text)  
    text= "SNORNA=annotations/" + org + "_snoRNAs.bed" + '\n'
    f.write(text)  
    f.close

def fadix(env_path, org, hum_vs_all):
    import subprocess
    import os    
    if hum_vs_all == 1:
        print("samtools faidx...")
        file_fa = org + ".fa"
        os.chdir(os.path.join(env_path, "Data", "annotations"))
        bashCommand = "samtools faidx " + file_fa
	print(bashCommand)
        output = subprocess.check_output(['bash','-c', bashCommand])
   

"""   
>human
CODING=annotations/hg38.gencode.v23.basic.coding.bed
CODING=annotations/hg38.gencode.v23.pseudo.bed
CODING=annotations/hg38.refseq.coding.bed
ORTHOLOG=chimp
LIFTOVER=annotations/hg38.panTro4.all.chain.gz
GENOME_FA=annotations/hg38.fa
DUPS=annotations/hg38.zfps.fa
DUPS=annotations/hg38.olfs.fa
NONCODING=annotations/hg38.gencode.v23.lncRNA.bed
GENESYMBOL=annotations/hg38.geneSymbolMap.txt
MIRNA=annotations/hg38.miRNAs.bed
SNORNA=annotations/hg38.snoRNAs.bed

>chimp
CODING=annotations/chimp_coding.bed
ORTHOLOG=human
#LIFTOVER=annotations/
GENOME_FA=annotations/chimp_genome.fa
NONCODING=annotations/chimp_lncRNAs.bed
GENESYMBOL=annotations/chimp_gene_names.txt
MIRNA=annotations/chimp_miRNAs.bed
SNORNA=annotations/chimp_snoRNAs.bed
"""
