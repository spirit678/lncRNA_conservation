# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import os
import subprocess
import shutil
####Maf conversion

from env_choose import env_choose
env_path = str(env_choose(2)) 
org = "CHIMP2.1.4"
folder = org
path = os.path.join(os.path.join(env_path, 'Downloads', 'maf', org))
#os.mkdir(path, 0777)
"""
os.mkdir(os.path.join(env_path, "Data", "MAF"), 0777)
os.mkdir(os.path.join(env_path, "Data", "PSL"), 0777)
os.mkdir(os.path.join(env_path, "Data", "MAF-gz"), 0777)
#########
base_dir = os.path.join(env_path, 'Downloads', "Temp")
os.chdir(base_dir)
bashCommand= "wget " + "ftp://ftp.ensembl.org/pub/release-84/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.toplevel.fa.gz"
print(bashCommand)
os.popen(bashCommand)
bashCommand= "wget " + "ftp://ftp.ensembl.org/pub/release-84/fasta/pan_troglodytes/dna/Pan_troglodytes.CHIMP2.1.4.dna.toplevel.fa.gz"
print(bashCommand)
os.popen(bashCommand)
sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
for i in sub_dirs:
    if i.endswith(".gz"):
        z= os.path.splitext(os.path.basename(i))[0]
        #name = os.path.basename(i)
        bashCommand = "gunzip " + "-c " + i + " > " + os.path.join(env_path, "Data", z)
        os.popen(bashCommand)
#########
base_dir = os.path.join(env_path, 'Downloads', 'maf', org)
os.chdir(base_dir)
bashCommand = "wget " + "ftp://ftp.ensembl.org/pub/release-85/maf/ensembl-compara/pairwise_alignments/homo_sapiens.GRCh38.vs.pan_troglodytes.CHIMP2.1.4.tar"
print(bashCommand)
os.popen(bashCommand)

sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
for i in sub_dirs:
    if i.endswith(".tar"):
        z0 = os.path.basename(i)
        z= os.path.splitext(os.path.basename(i))[0]
        print z0
        bashCommand = "tar -xf " + z0 + " -C " + os.path.join(env_path, "Data", "MAF-gz") 
        print(bashCommand)
        os.popen(bashCommand)
    else:
        print("No tar..or..")

base_dir = os.path.join(env_path, "Data", "MAF-gz", z)
os.chdir(base_dir)        
sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
for i in sub_dirs:
    if i.endswith(".gz"):
        z= os.path.splitext(os.path.basename(i))[0]
        #name = os.path.basename(i)
        bashCommand = "gunzip " + "-c " + i + " > " + os.path.join(env_path, "Data", "MAF", z)
        os.popen(bashCommand)

base_dir = os.path.join(env_path, "Data", "MAF")   
os.chdir(base_dir)
sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
for i in sub_dirs:
    if i.endswith(".maf"):        
        ## maf to psl
        z= os.path.splitext(os.path.basename(i))[0]
        name_f = z + ".psl"
        bashCommand = "maf-convert " + "psl " + i + " > " + os.path.join(env_path, "Data", "PSL", name_f)
        print(bashCommand)
        os.popen(bashCommand)
"""
base_dir = os.path.join(env_path, "Data", "PSL")
shutil.copy2(os.path.join(env_path, "src", "axtChain"), base_dir)   
os.chdir(base_dir)
sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
for i in sub_dirs:
    if i.endswith(".psl"):
        ## maf to psl
        z= os.path.splitext(os.path.basename(i))[0]
        name_f = z + ".chain"
        bashCommand = "./axtChain" + " -linearGap=medium " + "-minScore=3000" +  " -psl " + i  +  " -faQ " + os.path.join(env_path, "Data", "Pan_troglodytes.CHIMP2.1.4.dna.toplevel.fa") +  " -faT " + os.path.join(env_path, "Data", "Homo_sapiens.GRCh38.dna.toplevel.fa") + " " + name_f
        print(bashCommand)
        os.popen(bashCommand)


base_dir = os.path.join(env_path, "Data", "PSL")
os.chdir(base_dir)
## Join close alignments
shutil.copy2(os.path.join(env_path, "src", "chainMergeSort"), base_dir)
## Sort and combine
all_Chain_name= "hg38." + org + ".all.chain"
bashCommand = "./chainMergeSort " + os.path.join(env_path, "Data", "PSL") + "/*.chain "  +  "> " +  os.path.join(env_path, "Data", all_Chain_name) 
print(bashCommand)
os.popen(bashCommand)
base_dir = os.path.join(env_path, "Data")
os.chdir(base_dir)
bashCommand = "gzip" + " -c " + all_Chain_name + " > " + all_Chain_name + ".gz"
print(bashCommand)
os.popen(bashCommand)
