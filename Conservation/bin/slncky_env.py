# -*- coding: utf-8 -*-
"""
Created on Wed Sep  7 21:02:07 2016

@author: spirit678
"""
import os
import subprocess

def slncky_test(env_path, org, hum_vs_all, liftover_set, lift_pad, gap_open, gap_extanded,  no_orf, no_bg, min_noncoding, threads):
    print("Prepairing bed...")
    os.chdir(os.path.join(env_path, "bin"))
    from gtf_format_test import gtf_format_test_1000
    os.chdir(os.path.join(env_path, "Data"))
    gtf_format_test_1000("transcripts.gtf", "transcripts.bed", env_path=env_path)
    print("Prepairing bed...done")  
    os.chdir(os.path.join(env_path, "Data"))
    bashCommand = "head -n 5 transcripts.bed"
    output = subprocess.check_output(['bash','-c', bashCommand])
    print(output) 
    bashCommand = "wc -l transcripts.bed"
    output = subprocess.check_output(['bash','-c', bashCommand])
    print ("total number of transcripts", output) 
    print("slncky process your data...")
    if liftover_set == 0.1:
        liftover_set = ""
    else:
        liftover_set = " --minMatch=" + str(liftover_set)
    if lift_pad ==50000:
        lift_pad = ""
    else:
        lift_pad = " --pad=" + str(liftover_set)
    if gap_open ==1.0:
        gap_open= ""
    else:
        gap_open= " --gap_open=" + str(gap_open)
    if gap_extanded ==40:
        gap_extanded= ""
    else:
        gap_extanded= " --gap_extend=" + str(gap_extanded)
    if no_orf ==0:
        no_orf= ""
    else:
        no_orf= " --no_orf"
    if no_bg ==0:
        no_bg= ""
    else:
        no_bg= " --no_bg"
    if min_noncoding >= 0:
        min_non= " --min_noncoding="  + str(min_noncoding)
    else:
        min_non= ""
    if threads == 5:
        threads_f = ""
    else:
        threads_f= " --threads=" + str(threads)
    if hum_vs_all == 0:
        bashCommand = "./slncky.v1.0 --web --bedtools ../src/bedtools2/bin/ --no_filter " + threads_f + min_non + no_orf + no_bg + liftover_set + lift_pad + gap_open + gap_extanded + " transcripts.bed hg38 Human_test > log_slncky"
        output = subprocess.check_output(['bash','-c', bashCommand])
    else:    
        out_test = org + "_test"
        bashCommand = "./slncky.v1.0 --web --bedtools ../src/bedtools2/bin/ --no_filter " + threads_f + min_non + no_orf + no_bg + liftover_set + lift_pad + gap_open + gap_extanded + " transcripts.bed human " + out_test + " > log_slncky"
        output = subprocess.check_output(['bash','-c', bashCommand])
    print("Processing finished...")    
    
    
    
    

def slncky_main(env_path, org, hum_vs_all, liftover_set, lift_pad, gap_open, gap_extanded, no_orf, no_bg, min_noncoding, threads):
    print("Prepairing bed...")
    os.chdir(os.path.join(env_path, "bin"))
    from GTF2BED_3 import gtf_format_general
    os.chdir(os.path.join(env_path, "Data"))
    if hum_vs_all == 0:
        annotation ="annotations/hg38.fa"        
        gtf_format_general("transcripts.gtf", "transcripts_all.bed", annotation, env_path=env_path)
    else:
        #annotation ="annotations/" + org + ".fa"
        annotation ="annotations/hg38.fa"
        gtf_format_general("transcripts.gtf", "transcripts_all.bed", annotation, env_path=env_path)
    print("Prepairing bed...done")    
    bashCommand = "head transcripts_all.bed"
    output = subprocess.check_output(['bash','-c', bashCommand])
    print(output)
    bashCommand = "wc -l transcripts_all.bed"
    output = subprocess.check_output(['bash','-c', bashCommand])
    print ("total number of transcripts", output)
    if liftover_set == 0.1:
        liftover_set = ""
    else:
        liftover_set = " --minMatch=" + str(liftover_set)
    if lift_pad ==50000:
        lift_pad = ""
    else:
        lift_pad = " --pad=" + str(lift_pad)
    if gap_open ==1.0:
        gap_open= ""
    else:
        gap_open= " --gap_open=" + str(gap_open)
    if gap_extanded ==40:
        gap_extanded= ""
    else:
        gap_extanded= " --gap_extend=" + str(gap_extanded)
    if no_orf ==0:
        no_orfs= ""
    else:
        no_orfs= " --no_orf"
    if no_bg ==0:
        no_bgs= ""
    else:
        no_bgs= " --no_bg"
    if min_noncoding > 0:
        min_non= " --min_noncoding="  + str(min_noncoding)
    else:
        min_non= "" 
    if threads == 5:
        threads_f = ""
    else:
        threads_f= " --threads=" + str(threads)
    
    print("slncky process your data...")
    
    
    if hum_vs_all == 0:
        bashCommand = "./slncky.v1.0 --web --bedtools ../src/bedtools2/bin/ --lastz ../src/lastz-distrib/bin/ --no_filter " + threads_f + min_non + no_orfs + no_bgs + liftover_set + lift_pad + gap_open + gap_extanded + " transcripts_all.bed hg38 Human_all > log_slncky"
        print(bashCommand)        
        output = subprocess.check_output(['bash','-c', bashCommand])
    else:    
        out_all = org + "_all"
        bashCommand = "./slncky.v1.0 --web --bedtools ../src/bedtools2/bin/ --lastz ../src/lastz-distrib/bin/ --no_filter " + threads_f + min_non + no_orfs + no_bgs + liftover_set + lift_pad + gap_open + gap_extanded + " transcripts_all.bed human " + out_all + " > log_slncky"
        print(bashCommand)          
        output = subprocess.check_output(['bash','-c', bashCommand])
    print("Processing finished...")       
    
