#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 13:09:19 2016

@author: spirit678
"""
def pipeline(env_path, env, hum_vs_all, org, folder, target, liftover_set, lift_pad, gap_open, gap_extanded, no_orf, no_bg, min_noncoding, ncbi,threads):
    import os
    import subprocess
    #import datetime
    import shutil
    #import cgitb
    import sys
    ####### Log and report#######################
    
    class Logger(object):
        def __init__(self, filename="Default.log"):
            self.terminal = sys.stdout
            self.log = open(filename, "a")
    
        def write(self, message):
            self.terminal.write(message)
            self.log.write(message)
    
    if env == 0:
        lg1="_test"
    else:
        lg1="_all"
    if hum_vs_all == 0:
        lg2="Human"
    else:
        lg2=org
    if target== 1:
        lg3="_transcriptome"
    elif target == 2:
        lg3= "_lncRNA_set"
    lg= str("../results/" + folder + "_" + lg2 + lg1 + lg3 + ".log")
    sys.stdout = Logger(lg)
    
    #############################################
    print("###########################################")    
    print('Slncky pipeline')
    ####Arg debug
    """
    print ("Current work environment is ", env_path)
    if hum_vs_all == 1:
        print( "Query organizm is " +  org)
    if env == 1:
        print ("it is 1 (whole query dataset)")
    elif env == 0:
        print ("it is 0 (test query dataset, first 10000)")
    else:
        print ("WTF???")
    
    if target == 1:
        print ("Transcript source - whole transcriptome")
    elif target ==2:
        print ("Transcript source - only lncRNA Data")
    else:
        print ("Transcript source - Dataset should be defined")
        
        
    print("slncky current config:")
    if liftover_set != 0.1:
        print ("liftOver MinMatch " + str(liftover_set))
    else:
        print ("liftOver MinMatch is default: " + str(liftover_set))
    if lift_pad !=50000:
        print ("Number of basepairs up- and downstream of lncRNA loci " + str(lift_pad))
    else:
        print ("Number of basepairs up- and downstream of lncRNA loci is default: " + str(lift_pad))
    if gap_open !=1.0:
        print ("Gap open penalty to pass to lastz: " + str(gap_open))
    else:
        print ("Gap open penalty to pass to lastz is default: " + str(gap_open))
    if gap_extanded !=40:
        print ("Gap extend penalty to pass to lastz. " + str(gap_extanded))
    else:
        print ("Gap extend penalty to pass to lastz is default: " + str(gap_extanded))
    if no_orf == 0:
        print ("No_orf status OFF ")
    else:
        print ("No_orf status ON ")  
    if no_bg == 0:
        print ("no_bg status OFF ")
    else:
        print ("no_bg status ON ")     
    print("#####################################")
    ######Prepartion stage
    """
    from copytree import copytree   
    ####Copy slnky
    
    copytree(os.path.join(env_path, "bin/slncky-master"), os.path.join(env_path, "Data"))
    shutil.copy2(os.path.join(env_path, "Downloads", "Transcripts", "HEK293", "transcript_info.txt"), os.path.join(env_path, "Data"))
    if target == 1:
        shutil.copy2(os.path.join(env_path, "Downloads", "Transcripts", "HEK293", "Transcriptome", "transcripts.gtf"), os.path.join(env_path, "Data"))
    elif target == 2:
        shutil.copy2(os.path.join(env_path, "Downloads", "Transcripts", "HEK293", "lncRNAs", "transcripts.gtf"), os.path.join(env_path, "Data"))
    else:
        print ("Define dataset")
    
    
        
    #####Annotation instalation
    
    
    if hum_vs_all == 0:
        print("Preparing annotation:decompress")
        os.chdir(os.path.join(env_path, "Downloads", "spec/human"))
        bashCommand = "tar -xzvf annotations.tar.gz"
        output = subprocess.check_output(['bash','-c', bashCommand])
        os.chdir(os.path.join(env_path, "Data"))
        os.mkdir(os.path.join(env_path, "Data/annotations"), 0o777);
        print("Preparing annotation:copying")
        copytree(os.path.join(env_path, "Downloads/spec/human/annotations"), os.path.join(env_path, "Data/annotations"))
    
    if hum_vs_all == 1:
        print("Preparing human annotation:decompress")
        
        os.chdir(os.path.join(env_path, "Downloads", "spec/human"))
        bashCommand = "tar -xzvf annotations.tar.gz"
        output = subprocess.check_output(['bash','-c', bashCommand])
        os.chdir(os.path.join(env_path, "Data"))
        os.mkdir(os.path.join(env_path, "Data/annotations"), 0o777);
        print("Preparing human annotation:copying")
        copytree(os.path.join(env_path, "Downloads/spec/human/annotations"), os.path.join(env_path, "Data/annotations"))
        shutil.rmtree(os.path.join(env_path, "Downloads/spec/human/annotations"))
        #base_dir = os.path.join(env_path, 'Downloads', 'spec')
        #sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
        
        from data_download_format import data_download_new
        data_download_new(env_path, org)
        #print "Downloading data spicies data.. done"    
    
        from data_download_format import annotation_formatting
        annotation_formatting(org, env_path)
        from annotations import annotations
        from annotations_ncbi import annotations_ncbi
        from rewrite_genome import genome_re
        from make_cfg import make_cfg
        from make_cfg import fadix
        print("annotation bed creation...")
        if ncbi == 0:        
            print("ensembl data...")
            annotations(org,env_path)
            print("genome rewriting...")
        elif ncbi == 1:
            print("ncbi data...")
            annotations_ncbi(org, env_path)
        else:
            print("error with ncbi annotations")
	#>3 to >chr3 not needed anymore         
	#genome_re(env_path, org)

        print("fadix...")
        fadix(env_path, org, hum_vs_all)
        print("creation of the new cfg file...")
        make_cfg(env_path, org)
       
    print("Working environment created")
    
    
    #########Formatting and creation of the bed file
    ####Cheking and slnky
    print ("Slncky - job start...")
    if env == 1:
        from slncky_env import slncky_main
        slncky_main(env_path, org, hum_vs_all, liftover_set, lift_pad, gap_open, gap_extanded, no_orf, no_bg, min_noncoding, threads)
    elif env == 0:
        from slncky_env import slncky_test
        slncky_test(env_path, org, hum_vs_all, liftover_set,lift_pad, gap_open, gap_extanded, no_orf, no_bg, min_noncoding,threads)
    else:
        print ("env error")
    
    os.chdir(os.path.join(env_path, "Data"))
    
    
    """
    ####Parsing slnky
    if env == 1:
        from parse_slncky import parse_general
        print "parse slncky results for whole dataset..."
        if hum_vs_all == 1:   
            out_all = org + "_all"
        if hum_vs_all == 0:
            out_all = "Human_all"
        parse_general(out_all, "transcripts_all.bed", "HEK293_transcript_info.txt", env_path)
    elif env == 0:
        from parse_slncky import parse_general
        print "parse slncky results for test dataset..."
        if hum_vs_all == 1:    
            out_test = org + "_test"
        if hum_vs_all == 0:
            out_test = "Human_test"
        parse_general(out_test, "transcripts.bed", "HEK293_transcript_info.txt", env_path)
    else:
        print "WTF??? parse"
    print "Parsing complete"
    """

    
    print("task finished")
    
    
    
