# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 13:25:41 2016

@author: spirit678
"""

""" #Not usefull anymore 
def data_download(env_path, org):
    import os
    import sys
    import subprocess
    workdir = os.path.join(env_path, 'Downloads', 'spec')
    os.chdir(workdir)
    config_data = [org]    
    #config_data = ['Chimp', 'Baboon', 'Rhesus', 'Gorilla','Marmoset','GreenMonkey','Orangutan', 'CrabEating', 'Tarsier', 'Lemur', 'Bonobo']
    for i in config_data:
        if i == "Chimp":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Chimp")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Chimp"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Chimp"))
                print "Please put into the Chimp directory 3 files:"
                print "1.) Annotations in *.gtf.gz format" 
                print "2.) chained blastz alignments in *.chain.gz format"
                print "2.) chained blastz alignments in *.fa.gz format"  
                sys.exit("Input data not full")
                              
                print "downloading hg38/vsPanTro4"            
                #bashCommand = "wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsPanTro4/hg38.panTro4.all.chain.gz"
                bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.panTro4.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Chimp")               
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading CHIMP2.1.4.dna.toplevel.fa"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-84/fasta/pan_troglodytes/dna/Pan_troglodytes.CHIMP2.1.4.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading CHIMP2.1.4.84.gtf"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-84/gtf/pan_troglodytes/Pan_troglodytes.CHIMP2.1.4.84.gtf.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                
            else:
                print "Chimp data exists"
        elif i == "Baboon":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Baboon")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Baboon"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Baboon"))
                print "downloading hg38/vsPapAnu2"            
                #bashCommand = "wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsPapAnu2/hg38.papAnu2.all.chain.gz"
		bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.papAnu2.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Baboon")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading PapAnu2.0.dna.toplevel.fa"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-84/fasta/papio_anubis/dna/Papio_anubis.PapAnu2.0.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading /Papio_anubis.PapAnu2.0.84.gtf"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-84/gtf/papio_anubis/Papio_anubis.PapAnu2.0.84.gtf.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "Baboon data exists"
        elif i == "Rhesus":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Rhesus")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Rhesus"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Rhesus"))
                print "/hg38/vsRheMac8"            
                #bashCommand = "wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsRheMac8/hg38.rheMac8.all.chain.gz"
		bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.rheMac8.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Rhesus")                
		output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading MMUL_1.dna.toplevel.fa"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/fasta/macaca_mulatta/dna/Macaca_mulatta.Mmul_8.0.1.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading MMUL_1.84.gtf.gz"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/gtf/macaca_mulatta/Macaca_mulatta.Mmul_8.0.1.86.gtf.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "Rhesus data exists"
        elif i == "Gorilla":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Gorilla")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Gorilla"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Gorilla"))
                print "hg38/vsGorGor3"            
                #bashCommand = "wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsGorGor4/hg38.gorGor4.all.chain.gz"
		bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.gorGor3.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Gorilla")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading /Gorilla_gorilla.gorGor3.1.dna.toplevel.fa"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-84/fasta/gorilla_gorilla/dna/Gorilla_gorilla.gorGor3.1.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading Gorilla_gorilla.gorGor3.1.84.gtf"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-84/gtf/gorilla_gorilla/Gorilla_gorilla.gorGor3.1.84.gtf.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "Gorilla data exists"
        elif i == "Marmoset":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Marmoset")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Marmoset"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Marmoset"))
                print "hg38/vsGorGor3"            
                bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.calJac3.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Marmoset")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading /Gorilla_gorilla.gorGor3.1.dna.toplevel.fa"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/fasta/callithrix_jacchus/dna/Callithrix_jacchus.C_jacchus3.2.1.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading Gorilla_gorilla.gorGor3.1.84.gtf"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/gtf/callithrix_jacchus/Callithrix_jacchus.C_jacchus3.2.1.86.gtf.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "Marmoset data exists"
        elif i == "GreenMonkey":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "GreenMonkey")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "GreenMonkey"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "GreenMonkey"))
                print "hg38/chlSab2"            
                bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.calJac3.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "GreenMonkey")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading ---"            
                bashCommand = "wget "
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading ---"            
                bashCommand = "wget "
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "GreenMonkey data exists"
        elif i == "Orangutan":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Orangutan")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Orangutan"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Orangutan"))
                print "hg38/ponAbe2"            
                bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.ponAbe2.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Orangutan")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading ---"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/fasta/pongo_abelii/dna/Pongo_abelii.PPYG2.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading ---"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/gtf/pongo_abelii/Pongo_abelii.PPYG2.86.gtf.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "Orangutan data exists"
        elif i == "CrabEating":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "CrabEating")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "CrabEating"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "CrabEating"))
                print "hg38/ponAbe2"            
                bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.macFas5.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "CrabEating")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading ---"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/pre/fasta/dna/macaca_fascicularis/Macaca_fascicularis.MacFas_5.0.76.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading ---"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/pre/gtf/macaca_fascicularis/Macaca_fascicularis.MacFas_5.0.pre.gtf"
                output = subprocess.check_output(['bash','-c', bashCommand])
                bashCommand = "gzip Macaca_fascicularis.MacFas_5.0.pre.gtf"
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "CrabEating data exists"
        elif i == "Tarsier":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Tarsier")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Tarsier"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Tarsier"))
                print "hg38/ponAbe2"            
                bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.tarSyr1.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Tarsier")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading ---"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/fasta/tarsius_syrichta/dna/Tarsius_syrichta.tarSyr1.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading ---"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/gtf/tarsius_syrichta/Tarsius_syrichta.tarSyr1.86.gtf.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "Tarsier data exists"
        elif i == "Lemur":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Lemur")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Lemur"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Lemur"))
                print "hg38/micMur2"            
                bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.micMur2.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Lemur")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading ---"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/fasta/microcebus_murinus/dna/Microcebus_murinus.Mmur_2.0.dna.toplevel.fa.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading ---"            
                bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/gtf/microcebus_murinus/Microcebus_murinus.Mmur_2.0.86.gtf.gz"
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "Lemur data exists"
        elif i == "Bonobo":
            if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', "Bonobo")):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', "Bonobo"), 0777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', "Bonobo"))
                print "hg38/ponAbe2"            
                bashCommand = "cp " +  os.path.join(env_path, 'Downloads', 'MyChainDir', "hg38.panPan1.all.chain.gz") + " " + os.path.join(env_path, 'Downloads', 'spec', "Bonobo")
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "downloading ---"            
                bashCommand = ""
                output = subprocess.check_output(['bash','-c', bashCommand])
                print "Downloading ---"            
                bashCommand = ""
                output = subprocess.check_output(['bash','-c', bashCommand])
            else:
                print "Bonobo data exists"
        else:
            print('we have no download data')
   """         
####Data formatting
def annotation_formatting(org,env_path):
    import os
    #import subprocess
    import shutil
    base_dir = os.path.join(env_path, 'Downloads', 'spec', org)
    #os.mkdir(os.path.join(env_path, 'Data', 'annotations'), 0777)    
    work_dir = os.path.join(env_path, 'Data', 'annotations')
    os.chdir(base_dir)
    print("unpacking annotations...")
    sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
    for i in sub_dirs:
        if i.endswith("chain.gz"):
            inp_file = os.path.basename(i)
            print(inp_file)
            shutil.copy2(os.path.join(base_dir, inp_file), os.path.join(env_path, "Data", 'annotations'))
        else:
            z= os.path.splitext(os.path.basename(i))[0]
            #name = os.path.basename(i)
            print(z)
            bashCommand = "gunzip " + "-c " + i + " > " + os.path.join(env_path, 'Data', 'annotations', z)
            os.popen(bashCommand)
######New Data checking
def data_download_new(env_path, org):
    import os
    import sys
    import subprocess
    workdir = os.path.join(env_path, 'Downloads', 'spec')
    os.chdir(workdir)
    j = str(org) 
    if not os.path.exists(os.path.join(env_path, 'Downloads', 'spec', j)):
                os.mkdir(os.path.join(env_path, 'Downloads', 'spec', j), 0o777)
                os.chdir(os.path.join(env_path, 'Downloads', 'spec', j))
                print ("Please put into the ./Downloads", j," directory 3 files:")
                print ("1.) Annotations in *.gtf.gz format") 
                print ("2.) chained blastz alignments in *.chain.gz format")
                print ("2.) Sequence in *.fa.gz format")  
                sys.exit("Input data not full")
    
#annotation_formatting("Chimp","/home/spirit678/Work/Conservation/")
    
    
    
    
    
            
"""            
#Chimp
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsPanTro4/hg38.panTro4.all.chain.gz
wget ftp://ftp.ensembl.org/pub/release-84/fasta/pan_troglodytes/dna/Pan_troglodytes.CHIMP2.1.4.dna.toplevel.fa.gz
wget ftp://ftp.ensembl.org/pub/release-84/gtf/pan_troglodytes/Pan_troglodytes.CHIMP2.1.4.84.gtf.gz

#query: Baboon
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsPapAnu2/hg38.papAnu2.all.chain.gz
wget ftp://ftp.ensembl.org/pub/release-84/fasta/papio_anubis/dna/Papio_anubis.PapAnu2.0.dna.toplevel.fa.gz
wget ftp://ftp.ensembl.org/pub/release-84/gtf/papio_anubis/Papio_anubis.PapAnu2.0.84.gtf.gz
#query: Rhesus
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsRheMac8/hg38.rheMac8.all.chain.gz
wget ftp://ftp.ensembl.org/pub/release-86/fasta/macaca_mulatta/dna/Macaca_mulatta.Mmul_8.0.1.dna.toplevel.fa.gz
wget ftp://ftp.ensembl.org/pub/release-86/gtf/macaca_mulatta/Macaca_mulatta.Mmul_8.0.1.86.gtf.gz
#query: Gorilla
wget http://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsGorGor4/hg38.gorGor4.all.chain.gz
wget ftp://ftp.ensembl.org/pub/release-84/fasta/gorilla_gorilla/dna/Gorilla_gorilla.gorGor3.1.dna.toplevel.fa.gz
wget ftp://ftp.ensembl.org/pub/release-84/gtf/gorilla_gorilla/Gorilla_gorilla.gorGor3.1.84.gtf.gz
"""
