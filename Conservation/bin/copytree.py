# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 13:31:37 2016

@author: spirit678
"""
import shutil
import os

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)