# -*- coding: utf-8 -*-
"""
Created on Wed Feb 15 17:38:46 2017

@author: spirit678
"""

import os
#import csv
import sys
chrom= {} 

name_input= sys.argv[1]
name_chr= sys.argv[2]
org = sys.argv[3]
fa = sys.argv[4]
from env_choose import env_choose
env_path = str(env_choose(4))

path = (os.path.join(env_path, 'Ready', name_input))
path2 =(os.path.join(env_path, 'External', name_chr))
path3 =(os.path.join(env_path, 'External', fa))
nout= org + "_final.gtf"
nout2= org + ".fa"
out=open((os.path.join(env_path, 'Ready', nout)), "w")
out2=open((os.path.join(env_path, 'Ready', nout2)), "w")
#out2 = open("out.fa", "w")
f = open(path2, "r")
f2 = open(path, "r")
f3 = open(path3, "r")

for line in f:
	if not line.startswith("#"):
		if line.startswith("chr"):
			#print(line)
			line = line.strip().split('\t')
			chromosome = line[0]
			scaff = line[6]
			#print(chromosome)
			#print(scaff)
			if scaff not in chrom:
		    		chrom[scaff] = chromosome
			else:
		    		pass
		elif line.startswith("MT"):
			line = line.strip().split('\t')
			chromosome = line[0]
			scaff = line[6]
			#print(scaff)
			if scaff not in chrom:
		    		chrom[scaff] = chromosome
			else:
		    		pass
		else:
			line = line.strip().split('\t')
			if line[9].find('chr') == 0:
				chromosome = line[9]
				scaff = line[6]
				#print(scaff)
				if scaff not in chrom:
		    			chrom[scaff] = chromosome
				else:
		    			pass

def is_empty(any_structure):
    if any_structure:
        print('Structure is not empty.')
	for line in f2:
		l1 = line.strip().split('\t')
		scaff2 = l1[0]
		#print(scaff2)
		if scaff2 in chrom:
		    chromos = chrom.get(scaff2)
		    #print(scaff2)
		    #print(chromos)            
		    line.replace(scaff2, chromos)
		    #print(line.replace(scaff2, chromos))           
		    text = line.replace(scaff2, chromos)
		    out.write(text)	    
        else:
            pass
	for line in f3:
	    if  line[0] == '>':
		l1 = line.strip().split('>')
		scaff2 = l1[1]
		print(scaff2)
		if scaff2 in chrom:
		    chromos = chrom.get(scaff2)
		    #print(scaff2)
		    print(chromos)            
		    line.replace(scaff2, chromos)
		    #print(line.replace(scaff2, chromos))           
		    text = line.replace(scaff2, chromos)
		    out2.write(text)
		else:
		    out2.write(line)
	    else:
		out2.write(line)	
    else:
        print('Structure is empty.')
	for line in f2:       
		text = line
		out.write(text)	 
is_empty(chrom)



"""    
for line in f3:
    if  line[0] == '>':
        l1 = line.strip().split('>')
        scaff2 = l1[1]
        print(scaff2)
        if scaff2 in chrom:
            chromos = chrom.get(scaff2)
            print(scaff2)
            print(chromos)            
            line.replace(scaff2, chromos)
            print(line.replace(scaff2, chromos))           
            text = line.replace(scaff2, chromos)
            out2.write(text)
        else:
            out2.write(line)
    else:
        out2.write(line)
"""
        
out.close()
#out2.close()
f.close()
f2.close()
