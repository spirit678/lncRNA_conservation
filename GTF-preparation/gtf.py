# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 13:55:36 2017

@author: spirit678
"""


import os
import subprocess
from env_choose import env_choose
env_path = str(env_choose(4))
def michal_to_ensembl(org):
    dict_transcript = {}
    nf= org + "_lncrnas_filtered.gtf"
    nf2= org + "_lncrnas_data_filtered.txt"
    nout= org + "_formatted.gtf"
    f = open(os.path.join(env_path, "PREDICTIONS", org, nf), "r") 
    f2 = open(os.path.join(env_path,"PREDICTIONS", org,  nf2), "r")   
    out = open(os.path.join(env_path,"Ready", nout), "w")
    for line in f:
      if not line[0] == '#':
        line = line.strip().split('\t')
        if line[2] == 'exon':
          features = line[8]
          transcript_id = features.split('; ')[1]
          gene_id = features.split('; ')[0]
          gID = gene_id.split(' ')[1].replace('"', '')
          TID = transcript_id.split(' ')[1].replace('"', '')
          exon_id = features.split('; ')[2]
          oId = features.split('; ')[3]
          tss = features.split('; ')[4].split(' ')[1].replace('"', '')
          feature = gene_id + '; gene_version "1"; ' + transcript_id + '; transcript_version "1"; ' + exon_id + '; ' + 'gene_source "cufflinks";  ' + 'gene_biotype "lncRNA"; transcript_source "Cufflinks"; transcript_biotype "lncRNA"; exon_id '+ tss + '; exon_version "1";'
          write = line[0] + '\t' + line[1] + '\t' + line[2] +'\t'  + line[3]  + '\t' + line[4] + '\t' + line[5] +'\t' + line[6] +'\t' + line[7] +'\t' + feature + '\n' 
          out.write(write)
          if TID not in dict_transcript:
              dict_transcript[TID] = [gID]
          else:
              pass
    #print(dict_transcript.items())          
    for line in f2:
      if not line[0] == '#':
        line = line.strip().split('\t')        
        features = line[8]
        transcript_id = line[1]
        if transcript_id in dict_transcript:        
            gene_id = dict_transcript.get(transcript_id)
            gene_id ="gene_id" + ' "' + gene_id[0] + '"'
            transcript_id  ="transcript_id" + ' "' + transcript_id + '"'
            #print(gene_id)
        else:
            #print(transcript_id)
            pass
        feature = str(gene_id) + '; gene_version "1"; ' + str(transcript_id) + '; transcript_version "1"; ' + 'gene_source "cufflinks";  ' + 'gene_biotype "lncRNA"; transcript_source "Cufflinks"; transcript_biotype "lncRNA";'
        write2 = line[3] + '\t' + "Cufflinks" + '\t' + "transcript"  + '\t' + line[4]  + '\t' + line[5] + '\t' + "." + '\t' + line[6]  + '\t' + "."  + '\t' + feature + '\n' 
        #print(transcript_id)
        out.write(write2)
    out.close

           
#Running
env_path = str(env_choose(4)) 
monkey=[]
for i in os.listdir(os.path.join(env_path, "PREDICTIONS")):
            inp_file = os.path.basename(i)
            #print(inp_file)
            monkey.append(inp_file)
print(monkey)
#ensembl
#if not os.path.exists(os.path.join(env_path, "ensembl")):
#                os.mkdir(os.path.join(env_path, "ensembl"), 0777)
print("Formatting of lncRNAs..")                
for i in monkey:
    michal_to_ensembl(i)
    print(i)
print("Formatting of lncRNAs..Done")     
"""
    os.chdir(os.path.join(env_path, "ensembl"))
    if i == "pan":
        bashCommand = "wget ftp://ftp.ensembl.org/pub/release-84/gtf/pan_troglodytes/Pan_troglodytes.CHIMP2.1.4.84.gtf.gz"
        os.popen2(bashCommand)  
    elif i == "gorilla":
        bashCommand = "wget ftp://ftp.ensembl.org/pub/release-84/gtf/gorilla_gorilla/Gorilla_gorilla.gorGor3.1.84.gtf.gz"
        os.popen2(bashCommand)
    elif i == "macaca":
        bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/gtf/macaca_mulatta/Macaca_mulatta.Mmul_8.0.1.86.gtf.gz"
        os.popen2(bashCommand)
    elif i == "pongo":
        bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/gtf/pongo_abelii/Pongo_abelii.PPYG2.86.gtf.gz"
        os.popen2(bashCommand)
    elif i == "marmoset":
        bashCommand = "wget ftp://ftp.ensembl.org/pub/release-86/gtf/callithrix_jacchus/Callithrix_jacchus.C_jacchus3.2.1.86.gtf.gz"
        os.popen2(bashCommand)
    else:
        pass
        
    os.chdir(os.path.join(env_path, "ensembl"))
    bashCommand = "gunzip *.gz"
    os.popen2(bashCommand)
    files = os.listdir(os.path.join(env_path, "ensembl"))
    #print(files)
"""
    

