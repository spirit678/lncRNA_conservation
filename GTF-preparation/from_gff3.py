# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 17:46:23 2017

@author: spirit678
"""

import HTSeq
import sys
import os 

name_input= sys.argv[1]
name_org= sys.argv[2]
from env_choose import env_choose
env_path = str(env_choose(4))

path = (os.path.join(env_path, 'External', name_input))
gff_file = HTSeq.GFF_Reader( str(path), end_included=True )
nout= name_org + "_annotations.gtf"

out=open((os.path.join(env_path, 'Ready', nout)), "w")

count = 0
transcripts= {}
transcr_ID_conversion= {}
transcript_start={}
transcript_end={}
for feature in gff_file:
     if feature.type == "ncRNA":
         #print(feature.type)
         transcript_id = feature.attr['transcript_id']
         ID_trans = feature.attr['ID']
         if ID_trans not in transcr_ID_conversion:
             transcr_ID_conversion[ID_trans] = transcript_id
         else:
             transcr_ID_conversion[ID_trans].append(transcript_id)
     elif feature.type == "transcript":
         #print(feature.type)
         transcript_id = feature.attr['transcript_id']
         ID_trans = feature.attr['ID']
         if ID_trans not in transcr_ID_conversion:
             transcr_ID_conversion[ID_trans] = transcript_id
         else:
             transcr_ID_conversion[ID_trans].append(transcript_id)
     elif feature.type == "mRNA":
         #print(feature.type)
         transcript_id = feature.attr['transcript_id']
         ID_trans = feature.attr['ID']
         if ID_trans not in transcr_ID_conversion:
             transcr_ID_conversion[ID_trans] = transcript_id
         else:
             transcr_ID_conversion[ID_trans].append(transcript_id)
     else:
         pass
    
             

         
         
         
         
for feature in gff_file:
   count = count+1
   types = feature.type
   source =feature.source
   coord = str(feature.iv)
   chrom = coord.split(":")[0]
   chain = coord.split(":")[1].split("/")[1]
   start = coord.split(":")[1].split("/")[0].split("[")[1].split(")")[0].split(",")[0]
   end = coord.split(":")[1].split("/")[0].split("[")[1].split(")")[0].split(",")[1]
   print("..next")
   print(count)
   print(feature.type)   
   print(feature.attr)

   if feature.type == "gene":
       gene_id= feature.attr["gene"]
       gene_name=feature.attr["gene"]
       biotype=feature.attr['gbkey']
          
       text =  chrom + '\t' + source + '\t' + types + '\t' + start  + '\t' + end + '\t' + '.' + '\t' + chain + '\t' + '.' + '\t' + 'gene_id ' + gene_id + ';' + ' gene_version "2"; ' + 'gene_name ' + gene_name + '; ' + 'gene_source '  + source + '; ' +'gene_biotype ' +   biotype + ';' + '\n'
       out.write(text)
   elif feature.type == "transcript":
       if "exception" not in feature.attr:
               if "Note" not in feature.attr:       
                   gene_id= feature.attr["gene"]
                   gene_name=feature.attr["gene"]
                   biotype=feature.attr['gbkey']         
                   transcript_id = feature.attr['transcript_id']
                   product=feature.attr['product']
                   text =  chrom + '\t' + source + '\t' + types + '\t' + start  + '\t' + end + '\t' + '.' + '\t' + chain + '\t' + '.' + '\t' + 'gene_id ' + gene_id + ';' + ' gene_version "2"; ' + 'transcript_id ' + transcript_id +  '; transcript_version  "2"; ' + 'gene_name ' + gene_name + '; ' + 'gene_source '  + source + '; ' +'gene_biotype ' +   biotype + '; transcript_name "'  + product + '"; transcript_source ' + source + '; transcript_biotype ' + biotype + ';' + '\n'         
                   out.write(text)
   elif feature.type == "ncRNA":
       if "exception" not in feature.attr:
               if "Note" not in feature.attr:
                   gene_id= feature.attr["gene"]
                   gene_name=feature.attr["gene"]
                   biotype=feature.attr['gbkey']      
                   transcript_id = feature.attr['transcript_id']
                   product=feature.attr['product']
                   text =  chrom + '\t' + source + '\t' + 'transcript' + '\t' + start  + '\t' + end + '\t' + '.' + '\t' + chain + '\t' + '.' + '\t' + 'gene_id ' + gene_id + ';' + ' gene_version "2"; ' + 'transcript_id ' + transcript_id +  '; transcript_version  "2"; ' + 'gene_name ' + gene_name + '; ' + 'gene_source '  + source + '; ' +'gene_biotype ' +   biotype + '; transcript_name "'  + product + '"; transcript_source ' + source + '; transcript_biotype ' + biotype + ';' + '\n'         
                   out.write(text)
   elif feature.type == "mRNA":
       if "exception" not in feature.attr:
               if "Note" not in feature.attr:       
                   gene_id= feature.attr["gene"]
                   gene_name=feature.attr["gene"]
                   biotype=feature.attr['gbkey']  
                   transcript_id = feature.attr['transcript_id']
                   product=feature.attr['product']
                   text =  chrom + '\t' + source + '\t' + 'transcript' + '\t' + start  + '\t' + end + '\t' + '.' + '\t' + chain + '\t' + '.' + '\t' + 'gene_id ' + gene_id + ';' + ' gene_version "2"; ' + 'transcript_id ' + transcript_id +  '; transcript_version  "2"; ' + 'gene_name ' + gene_name + '; ' + 'gene_source '  + source + '; ' +'gene_biotype ' +   biotype + '; transcript_name  "'  + product + '"; transcript_source ' + source + '; transcript_biotype ' + biotype + ';' + '\n'         
                   out.write(text)
   elif feature.type == "exon":
       if "exception" not in feature.attr:
               if "Note" not in feature.attr:
                   if feature.attr["gbkey"] != "tRNA":# or "Note":
                       gene_id= feature.attr["gene"]
                       gene_name=feature.attr["gene"]
                       biotype=feature.attr['gbkey']
                       trans_id = feature.attr['Parent']
                       if trans_id in transcr_ID_conversion:
                           transcript_id = transcr_ID_conversion.get(trans_id)
                       else:
                           transcript_id = feature.attr['Parent']                           
                       IDs = feature.attr['ID']
                       product=feature.attr['gene']
                       text =  chrom + '\t' + source + '\t' + types + '\t' + start  + '\t' + end + '\t' + '.' + '\t' + chain + '\t' + '.' + '\t' + 'gene_id ' + gene_id + ';' + ' gene_version "2"; ' + 'transcript_id ' + transcript_id +  '; transcript_version  "2"; ' + 'exon_number "1"; ' +'gene_name ' + gene_name + '; ' + 'gene_source '  + source + '; ' +'gene_biotype ' +   biotype + '; transcript_name  "'  + product + '"; transcript_source ' + source + '; transcript_biotype ' + biotype + '; ' + 'exon_id ' + IDs + '; exon_version "1"; ' +  '\n'
                       out.write(text)
   elif feature.type == "CDS":
       if "exception" not in feature.attr:
           if "Note" not in feature.attr:
               #if feature.attr["gbkey"] != "tRNA":
               gene_id= feature.attr["gene"]
               gene_name=feature.attr["gene"]
               biotype=feature.attr['gbkey']  
               #transcript_id = feature.attr['Parent']
               trans_id = feature.attr['Parent']
               if trans_id in transcr_ID_conversion:
                   transcript_id = transcr_ID_conversion.get(trans_id)
               else:
                   transcript_id = feature.attr['Parent']                 
               product=feature.attr['product']
               protein_id=feature.attr['protein_id']
               text =  chrom + '\t' + source + '\t' + types + '\t' + start  + '\t' + end + '\t' + '.' + '\t' + chain + '\t' + '.' + '\t' + 'gene_id ' + gene_id + ';' + ' gene_version "2"; ' + 'transcript_id ' + transcript_id +  '; transcript_version  "2"; ' + 'exon_number "1"; ' +'gene_name ' + gene_name + '; ' + 'gene_source '  + source + '; ' +'gene_biotype ' +   biotype + '; transcript_name  "'  + product + '"; transcript_source ' + source + '; transcript_biotype ' + biotype + '; ' + 'protein_id ' + protein_id + '; exon_version "1"; ' +  '\n'
               out.write(text)
   else:
       pass
out.close()
   





















"""
transcripts= {}
transcript_start={}
transcript_end={}
for feature in gff_file:
   if feature.type == "exon":
      transcript_id = feature.attr['Parent']
      print feature
      print(feature.attr)
      
      if transcript_id not in transcripts:
         transcripts[ transcript_id ] = list()
      transcripts[ transcript_id ].append( feature )
     
for transcript_id in sorted( transcripts ):      
   transcript_length = 0
   for exon in transcripts[ transcript_id ]:
      transcript_length += exon.iv.length + 1
   print transcript_id, transcript_length, len( transcripts[ transcript_id ] )
   
   
   
   
   


for k, v in d.items():
    print k, max(v)

"""
transcripts_bio= {}
gff={}
for feature in gff_file:
    gff_types = feature.type
    try:
        i = feature.attr["gbkey"] 
        if i not in transcripts_bio:
            transcripts_bio[i] = list()
        else:
            pass
    except:
        pass
    if gff_types not in gff:
        gff[gff_types] = list() 
        
print("cheking ref gbkey...") 
for key in transcripts_bio.iteritems() :
    print ("gbkey is ", key)
for key in gff.iteritems() :
    print ("feature.types ", key)
check={}
count=0
f=open( path, "r")
for line in f:
    if not line[0] == '#':
        line = line.strip().split('\t')
        types=line[2]
        
        if types not in check:
            check[types] = types
print("cheking ref types...") 
for key in check.iteritems() :
    print key


check2={}
count=0
f=open((os.path.join(env_path, 'Ready', nout)), "r")
for line in f:
    if not line[0] == '#':
        line = line.strip().split('\t')
        types2=line[2]
        if types2 not in check2:
            check2[types2] = types
print("cheking output types...")
for key in check2.iteritems() :
    print key
 

""" 
check={}
count=0
f=open( "Pan_troglodytes.CHIMP2.1.4.84.gtf", "r")
for line in f:
    if not line[0] == '#':
        line = line.strip().split('\t')
        types=line[2]
        
        if types not in check:
            check[types] = types
            print types
          
check={}
count=0
f=open( "test2.gtf", "r")
for line in f:
    line = line.strip().split('\t')
    types=line[2]
    print(line)
    if types not in check:
        check[types] = types
        print types
print("cheking...") 
"""  
