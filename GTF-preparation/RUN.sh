#!/bin/bash
####Config
#ANNOTATIONS=ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/258/655/GCF_000258655.2_panpan1.1/GCF_000258655.2_panpan1.1_genomic.gff.gz
#ASSEMBLY_REP=ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/258/655/GCF_000258655.2_panpan1.1/GCF_000258655.2_panpan1.1_assembly_report.txt
#ASSEMBLY_FNA=ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/258/655/GCF_000258655.2_panpan1.1/GCF_000258655.2_panpan1.1_genomic.fna.gz
#ORG=paniscus
#TYPE=NCBI

####Example saebus
ANNOTATIONS=ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/409/795/GCF_000409795.2_Chlorocebus_sabeus_1.1/GCF_000409795.2_Chlorocebus_sabeus_1.1_genomic.gff.gz
ASSEMBLY_REP=ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/409/795/GCF_000409795.2_Chlorocebus_sabeus_1.1/GCF_000409795.2_Chlorocebus_sabeus_1.1_assembly_report.txt
ASSEMBLY_FNA=ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/409/795/GCF_000409795.2_Chlorocebus_sabeus_1.1/GCF_000409795.2_Chlorocebus_sabeus_1.1_genomic.fna.gz
HUM_FA=http://rhesus.amu.edu.pl/alexey/hg38.fa.gz
ORG=sabaeus
TYPE=NCBI

####Example Chimp
#ANNOTATIONS=ftp://ftp.ensembl.org/pub/release-90/gtf/pan_troglodytes/Pan_troglodytes.CHIMP2.1.4.90.gtf.gz
#ASSEMBLY_FNA=ftp://ftp.ensembl.org/pub/release-90/fasta/pan_troglodytes/dna/Pan_troglodytes.CHIMP2.1.4.dna.toplevel.fa.gz
#HUM_FA=http://rhesus.amu.edu.pl/alexey/hg38.fa.gz
#ORG=pan
#TYPE=ensembl

DIR=$(pwd)
#Job
FORM=_formatted.gtf
if [ ! -f ./Ready/$ORG$FORM ]; then
    python2.7 gtf.py
fi
#main
if [ "$TYPE" = NCBI ]; then         
	#local HTSeq
	pip2.7 install --user HTSeq
	a=_report
	if [[ "$ANNOTATIONS" == *.gff.gz ]]; then
		#Downloading
		wget $ANNOTATIONS -O ./External/$ORG.gff.gz
		wget $ASSEMBLY_FNA -O ./External/$ORG.fna.gz
		wget $ASSEMBLY_REP  -O ./External/$ORG$a
		gunzip ./External/$ORG.gff.gz
		gunzip ./External/$ORG.fna.gz
		#Gff formating
		python2.7 from_gff.py $ORG.gff $ORG
	elif [[ "$ANNOTATIONS" == *.gff3.gz ]]; then
		#Downloading
		wget $ANNOTATIONS -O ./External/$ORG.gff3.gz
		wget $ASSEMBLY_FNA -O ./External/$ORG.fna.gz
		wget $ASSEMBLY_REP  -O ./External/$ORG$a
		gunzip ./External/$ORG.gff3.gz
		gunzip ./External/$ORG.fna.gz
		#Gff formating
		python2.7 from_gff.py $ORG.gff3 $ORG		
	fi
	#FNA to fa
	python2.7 fa.py $ORG.fna $ORG
	#Concatenation
	b=_annotations.gtf
	c=_formatted.gtf
	d=_final.gtf
	cat ./Ready/$ORG$b ./Ready/$ORG$c > ./Ready/$ORG.gtf

	python2.7 convert.py $ORG.gtf $ORG$a $ORG $ORG.fa

	cp ./Ready/$ORG$d ./Testing/Data/annotations/
	python2.7 ./Testing/annotations_ncbi.py
	head ./Testing/Data/annotations/*.bed
	gzip ./Ready/$ORG$d
	
	rm -f ./Ready/$ORG$b
	#rm -f ./Ready/$ORG.gtf
	#Chain
	grep ">" ./Ready/$ORG.fa
	./faToTwoBit ./Ready/$ORG.fa ./Chain/Data/$ORG.2bit
	gzip ./Ready/$ORG.fa
	wget $HUM_FA -O ./External/hg38.fa.gz
	gunzip ./External/hg38.fa.gz
	grep ">" ./External/hg38.fa
	./faToTwoBit ./External/hg38.fa ./Chain/Data/hg38.2bit
	cp ./BatchJobs.R ~/
	cp ./SGETemplate.tmpl ~/	
	cd ./Chain/
	Rscript ./ParallelPairwiseAlignment.R $DIR/Chain/Data/hg38.2bit $DIR/Chain/Data/$ORG.2bit $DIR/Chain/scripts
	gzip ./hg38.$ORG.all.chain
	cd ../
	mkdir ./$ORG
	mv ./Chain/hg38.$ORG.all.chain.gz ./$ORG
	mv ./Ready/$ORG.fa.gz ./$ORG
	mv ./Ready/$ORG$d.gz ./$ORG
	
else
	#extracting
	#wget $ANNOTATIONS -O ./External/$ORG.gtf.gz
	#gunzip ./External/$ORG.gtf.gz
	#wget $ASSEMBLY_FNA -O ./External/$ORG.fa.gz
	#gunzip ./External/$ORG.fa.gz
	#Concatenation
	c=_formatted.gtf
	d=_final.gtf
	cat ./External/$ORG.gtf ./Ready/$ORG$c > ./Ready/$ORG.gtf

	python2.7 ./convert_ensembl.py $ORG.gtf $ORG $ORG.fa

	#checking
	cp ./Ready/$ORG$d ./Testing/Data/annotations/
	python2.7 ./Testing/annotations.py 
	head ./Testing/Data/annotations/*.bed
	gzip ./Ready/$ORG$d 
	#Chain
	#mv ./External/$ORG.fa ./Ready/$ORG.fa
	grep ">" ./Ready/$ORG.fa
	./faToTwoBit ./Ready/$ORG.fa ./Chain/Data/$ORG.2bit
	wget $HUM_FA -O ./External/hg38.fa.gz
	gunzip ./External/hg38.fa.gz
	./faToTwoBit ./External/hg38.fa ./Chain/Data/hg38.2bit
	gzip ./Ready/$ORG.fa
	cp ./BatchJobs.R ~/
	cp ./SGETemplate.tmpl ~/	
	cd ./Chain/
	Rscript ./ParallelPairwiseAlignment.R $DIR/Chain/Data/hg38.2bit $DIR/Chain/Data/$ORG.2bit $DIR/Chain/scripts
	gzip ./hg38.$ORG.all.chain
	cd ../
	mkdir ./$ORG
	mv ./Ready/$ORG.fa.gz ./$ORG
	mv ./Chain/hg38.$ORG.all.chain.gz ./$ORG
	mv ./Ready/$ORG$d.gz ./$ORG

fi
rm -f ./External/*.*
rm -f ./External/*
rm -f ./Testing/Data/annotations/*.*
rm -f ./Chain/lav/*
rm -f ./Chain/psl/*
rm -f ./Chain/chain/*
rm -f ./Chain/Data/*
rm -f ./Ready/*
rmdir ./Chain/lav/
rmdir ./Chain/psl/
rmdir ./Chain/chain/
