
def annotations_ncbi(org):
    import os
    from env_choose import env_choose
    env_path = str(env_choose(4))
    base_dir = os.path.join(env_path,'Testing', 'Data', 'annotations')
    sub_dirs = [os.path.join(base_dir, d) for d in os.listdir(base_dir)]
    for file in sub_dirs:
        if file.endswith(".gtf"):
                inp_file = file
		print("Annotations checking")
                print(inp_file)
                inp_fi = os.path.join(env_path,'Testing', "Data", "annotations", str(inp_file))
    #inp(env_path)        
    #path = os.path.join(env_path, 'Data', 'annotations')
    species = org
    os.chdir(os.path.join(env_path,'Testing', "Data", "annotations"))
    inp_fi = os.path.join(env_path,'Testing', "Data", "annotations", str(inp_file))
        
    f = open(inp_fi, "r")
    out1 = open(os.path.join(env_path,'Testing', 'Data', 'annotations', (species + '_coding.bed')), 'w') # protein_coding
    out2 = open(os.path.join(env_path,'Testing', 'Data', 'annotations', (species + '_snoRNAs.bed')), 'w') # snoRNAs
    out3 = open(os.path.join(env_path,'Testing', 'Data', 'annotations', (species + '_miRNAs.bed')), 'w') # miRNAs
    out4 = open(os.path.join(env_path,'Testing', 'Data', 'annotations', (species + '_gene_names.txt')), 'w') # gene_names
    out5 = open(os.path.join(env_path,'Testing', 'Data', 'annotations', (species + '_lncRNAs.bed')), 'w') # lncRNAs (including pseudogenes)
    
    
    #lncrna_biotypes = ['unitary_pseudogene', 'lincRNA', 'antisense', 'pseudogene', 'sense_overlapping', 'transcribed_unitary_pseudogene', 'sense_intronic', 'unprocessed_pseudogene', 'transcribed_unprocessed_pseudogene', 'translated_unprocessed_pseudogene', 'macro_lncRNA', 'retained_intron', 'processed_transcript', 'transcribed_processed_pseudogene', '3prime_overlapping_ncrna', 'processed_pseudogene', "lncRNA",'ncRNA']
    lncrna_biotypes = ["lncRNA"]
    global count
    dict_exon_starts = {}
    dict_exon_lengths = {}
    dict_data = {}
    for line in f:
      if not line[0] == '#':
        line = line.strip().split('\t')
        start = int(line[3])+1
        count = count+1
        #print(line)
        #print(line[4])
        #print(count)
        #print(line)
        end = int(line[4])
        strand = line[6]
        features = line[8]
        if line[2] == 'exon':
          features = features.split('; ')
          for elem in features:
            if "transcript_id" in elem:
              transcript_id = elem.split(' ')[1]
          if transcript_id in dict_exon_starts:
            dict_exon_starts[transcript_id].append(start)
          else:
            dict_exon_starts[transcript_id] = [start]
          length = abs(end-start)
          if transcript_id in dict_exon_lengths:
            dict_exon_lengths[transcript_id].append(length)
          else:
            dict_exon_lengths[transcript_id] = [length]
        if line[2] == 'transcript':
          features = features.split('; ')
          gene_id, transcript_id, gene_name, transcript_biotype = 'none', 'none', 'none', 'none'
          for elem in features:
            if "gene_id" in elem:
              gene_id = elem.split(' ')[1]
            if "transcript_id" in elem:
              transcript_id = elem.split(' ')[1]
              #print(transcript_id)
            if "gene_name" in elem:
              gene_name = elem.split(' ')[1] 
            if "transcript_biotype" in elem:
        	try:
			transcript_biotype = elem.split(' ')[1].replace('"', '').replace(';', '').strip() 
			#print(transcript_biotype)     
		except:
			transcript_biotype = elem.split(' ')[1].strip(";")
              		#print(transcript_biotype)
          dict_data[transcript_id] = (gene_id, gene_name, transcript_biotype)
            
            
    f.close()
    f = open(inp_file, "r")
    
    
    for line in f:
      if not line[0] == '#':
        line = line.strip().split('\t')
        if line[2] == 'transcript':
          if len(line[0]) < 3 and line[0] != 'MT':
            chr = 'chr' + line[0]
          else:
            chr = line[0]
          start = str(int(line[3]) + 1)
          end = str(int(line[4]))
          strand = line[6]
          features = line[8].split('; ')
          for elem in features:
            if "transcript_id" in elem:
              transcript_id = elem.split(' ')[1]
	      try:
	  	trans_id = elem.split(' ')[1].replace('"', '').replace(';', '').strip()      
	      except:
		trans_id = elem.split(' ')[1].strip(";")
          exon_starts = dict_exon_starts[transcript_id]
          exon_lengths = dict_exon_lengths[transcript_id]
          s = int(start)
          exon_starts = [elem - s for elem in exon_starts]
          exon_lengths = [str(elem) for elem in exon_lengths]
          exon_starts = [str(elem) for elem in exon_starts]
          block_count = len(exon_starts)
          exon_starts = ','.join(exon_starts)
          exon_lengths = ','.join(exon_lengths)
          write = chr + '\t' + start + '\t' + end + '\t' + trans_id + '\t0\t' + strand + '\t' + str(start) + '\t' + str(end) + '\t0\t' + str(block_count) + '\t' + exon_lengths + '\t' + exon_starts + '\n'
          data = dict_data[transcript_id]
          gene_id, gene_name, transcript_biotype = data
          if transcript_biotype == 'mRNA':
            out1.write(write)
          if transcript_biotype == 'snoRNA':
            out2.write(write)
          if transcript_biotype == 'miRNA':
            out3.write(write)
          out4.write(transcript_id + '\t' + gene_name + '\n')
          if transcript_biotype in lncrna_biotypes:
            out5.write(write)      
count=0            
annotations_ncbi("Test")          
